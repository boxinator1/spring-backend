package se.experis.com.backend.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.stream.Collectors;

public class RequestHandler {
    //Load from config?

    private final int TRIES_TIMEOUT_MIN = 5;
    private final int BLACKLIST_TIMEOUT_MIN = 15;

    private final int MAX_TRIES = 5;
    public static RequestHandler requestHandler = null;
    private HashMap<String, ArrayList<Date>> triesList;
    private HashMap<String, Date> blackList;

    public RequestHandler() {
        triesList = new HashMap<>();
        blackList = new HashMap<>();
    }
    public static RequestHandler getInstance() {
        if(requestHandler == null) {
            requestHandler = new RequestHandler();
        }
        return requestHandler;
    }

    /**
     * Adds a new try and filter out each try that has passed.
     * @param ipAddress
     */
    public void addTry(String ipAddress) {
        ArrayList<Date> tries = triesList.get(ipAddress);
        if(tries != null) {
            tries = (ArrayList<Date>) tries.stream().filter(d -> d.after(new Date())).collect(Collectors.toList());
            if(tries.size() >= MAX_TRIES) {
                setBlackListed(ipAddress);
            }
            else {

                tries.add(new Date(System.currentTimeMillis()+(1000*60*TRIES_TIMEOUT_MIN)));
            }
        }
        else {
            tries = new ArrayList<>();
            tries.add(new Date(System.currentTimeMillis()+(1000*60*TRIES_TIMEOUT_MIN)));

        }
        triesList.put(ipAddress, tries);
    }
    private void setBlackListed(String ipAddress) {
        blackList.put(ipAddress, new Date(System.currentTimeMillis()+(1000*60*BLACKLIST_TIMEOUT_MIN)));
    }
    public boolean isBlackListed(String ipAddress) {
        Date expiersAt = blackList.get(ipAddress);
        if(expiersAt == null) return false;
        if(expiersAt.before(new Date())) {
            System.out.println("here");
            blackList.remove(ipAddress);
        }
        return blackList.get(ipAddress) != null;
    }

}

