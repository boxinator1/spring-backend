package se.experis.com.backend.util;

import se.experis.com.backend.model.Package;

public class PackageValidator {

    private static PackageValidator packageValidator = null;

    public static PackageValidator getInstance() {
        if (packageValidator == null) {
            packageValidator = new PackageValidator();
        }
        return packageValidator;
    }

    //Displays correct messages if body contains errors.
    public String validatePackage(Package shipment) {
        StringBuilder stringBuilder = new StringBuilder();

        if (shipment.weight <=0 || shipment.weight >10000) {
            stringBuilder.append("Invalid weight\n");
        }
        if(!Countries.getInstance().contains(shipment.destination)) {
            stringBuilder.append("Invalid country \n ");
        }
        if(!isValidEmailAddress(shipment.email)){
            stringBuilder.append("Invalid email address \n");
        }
        if(!isValidColor(shipment.boxColor)){
            stringBuilder.append("Invalid color \n");
        }
        if(shipment.receiver == null || shipment.receiver.length() < 2) {
            stringBuilder.append("Invalid receiver \n");
        }
        return stringBuilder.toString();
    }

    private boolean isValidEmailAddress(String email) {
        if(email == null) { return false;}
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }

    public boolean isValidColor (String color) {
        if(color == null) { return false;}
        String ePattern = "^#[0-9a-fA-F]{6}$";

        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(color);

        return m.matches();
    }

}