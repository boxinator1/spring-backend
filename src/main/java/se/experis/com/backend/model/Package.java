package se.experis.com.backend.model;


import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import se.experis.com.backend.enums.PackageStatus;

import javax.persistence.*;
import java.util.Date;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Package {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    public Integer id;

    @Column
    public String email;

    @Column(nullable = false)
    public String receiver;

    @Column(nullable = false)
    public PackageStatus status;

    @Column(nullable = false)
    public Float weight;

    @Column(nullable = false)
    public String boxColor;

    @Column(nullable = false)
    public String destination;

    @Column(nullable = false)
    public Date date;

    @Column
    public int cost;

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name="account_id", nullable=false)
    public Account account;

}
