package se.experis.com.backend.restcontroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import se.experis.com.backend.model.Account;
import se.experis.com.backend.model.CommonResponse;
import se.experis.com.backend.model.Country;
import se.experis.com.backend.repositories.CountryRepository;
import se.experis.com.backend.util.Countries;
import se.experis.com.backend.enums.AccountType;

import javax.servlet.http.HttpServletRequest;

@CrossOrigin //TODO REMOVE
@RestController
/*
 * Handling APIs concerning the Settings, only for Admins.
 */

public class SettingsController {
    @Autowired
    private CountryRepository countryRepository;

    /**
     * This endpoint aims to get the information from all the countries currently in the db
     *
     * @param request - For authentication
     * @return - A response entity containing the {@link se.experis.com.backend.model.CommonResponse} which holds a data and an explaining message. Also the HTTP status.
     * @STATUS-200 - OK
     * @STATUS-401 - Unauthorized
     */
    @RequestMapping(value = "/settings/countries", method = RequestMethod.GET)
    public ResponseEntity<CommonResponse> getAllCountryInformation(HttpServletRequest request) {
        CommonResponse commonResponse = new CommonResponse();
        HttpStatus status = HttpStatus.UNAUTHORIZED;

        Account account = (Account) request.getAttribute("account");

        if (account != null && account.accountType == AccountType.ADMINISTRATOR) {
            status = HttpStatus.OK;
            commonResponse.data = countryRepository.findAll();
            commonResponse.message = "All country settings displayed";

        } else {
            commonResponse.message = "Not authorized";
        }
        return new ResponseEntity<>(commonResponse, status);
    }

    /**
     * This endpoint aims to create a new country to the database.
     *
     * @param country - Body containing the country information
     * @param request - for authentication
     * @return - A response entity containing the {@link se.experis.com.backend.model.CommonResponse} which holds a data and an explaining message. Also the HTTP status.
     * @STATUS-201 - Created
     * @STATUS-400 - Bad request
     * @STATUS-401 - Unauthorized
     */
    @RequestMapping(value = "/settings/countries", method = RequestMethod.POST)
    public ResponseEntity<CommonResponse> addNewCountryInformation(@RequestBody Country country,
                                                                   HttpServletRequest request) {
        CommonResponse commonResponse = new CommonResponse();

        HttpStatus status = HttpStatus.UNAUTHORIZED;
        Account account = (Account) request.getAttribute("account");

        if (account != null && account.accountType == AccountType.ADMINISTRATOR) {

            Boolean packageValidator = Countries.getInstance().contains(country.countryName);

            if (account.accountType == AccountType.ADMINISTRATOR) {
                if (packageValidator && countryRepository.getAllByCountryName(country.countryName) == null) {
                    if (country.multiplier >= 1) {
                        status = HttpStatus.CREATED;
                        country = countryRepository.save(country);
                        commonResponse.data = country;
                        commonResponse.message = "Country was CREATED";
                    } else {
                        commonResponse.message = "Wrong multiplier";
                        status = HttpStatus.BAD_REQUEST;
                    }
                } else {
                    commonResponse.message = "Wrong country";
                    status = HttpStatus.BAD_REQUEST;
                }
            } else {
                commonResponse.message = "Not authorized";
                status = HttpStatus.UNAUTHORIZED;
            }
        }
        return new ResponseEntity<>(commonResponse, status);
    }

    /**
     * This endpoint aims to change the multiplier-information on one of the the database.
     * Admin privileges are required. The api currently only checks for user
     *
     * @param countryMultiplier - Body containing the updated country-information (multiplier)
     * @param id                - Path variable containing the id for specific country.
     * @param request           - for authentication
     * @return - A response entity containing the {@link se.experis.com.backend.model.CommonResponse} which holds a data and an explaining message. Also the HTTP status.
     * @STATUS-201 - Created
     * @STATUS-400 - Bad request
     * @STATUS-401 - Unauthorized
     */
    @RequestMapping(value = "/settings/countries/{id}", method = RequestMethod.PATCH)
    public ResponseEntity<CommonResponse> changeCountryInformation(@RequestBody Float countryMultiplier,
                                                                   @PathVariable Integer id,
                                                                   HttpServletRequest request) {
        CommonResponse commonResponse = new CommonResponse();

        HttpStatus status = HttpStatus.UNAUTHORIZED;
        Account account = (Account) request.getAttribute("account");

        if (account != null && account.accountType == AccountType.ADMINISTRATOR) {

            Country country = countryRepository.getCountryById(id);

            if (country != null) {
                if (countryMultiplier >= 1) {
                    status = HttpStatus.ACCEPTED;
                    country.multiplier = countryMultiplier;
                    country = countryRepository.save(country);

                    commonResponse.data = country;
                    commonResponse.message = "Country was UPDATED";
                } else {
                    commonResponse.message = "Wrong multiplier";
                    status = HttpStatus.BAD_REQUEST;
                }
            } else {
                commonResponse.message = "Wrong country";
                status = HttpStatus.BAD_REQUEST;
            }
        } else {
            commonResponse.message = "Not authorized";
        }

        return new ResponseEntity<>(commonResponse, status);
    }

    /**
     * This endpoint aims to delete a country.
     *
     * @param id - Pathvariable containing the id.
     * @STATUS-200 - OK if resource was fetched and deleted.
     * @STATUS-401 - Unauthorized
     * @STATUS-404 - Not Found if the server can't find the requested resource
     */
    @RequestMapping(value = "/settings/countries/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<CommonResponse> changeCountryInformation(@PathVariable Integer id,
                                                                   HttpServletRequest request) {
        CommonResponse commonResponse = new CommonResponse();
        HttpStatus status = HttpStatus.UNAUTHORIZED;

        Account account = (Account) request.getAttribute("account");

        if (account != null && account.accountType == AccountType.ADMINISTRATOR) {
            Country country = countryRepository.getCountryById(id);

            if (country != null) {
                countryRepository.deleteById(id);
                commonResponse.message = "Country was deleted";
                status = HttpStatus.OK;
            } else {
                commonResponse.message = "Wrong country";
                status = HttpStatus.NOT_FOUND;
            }
        }
        return new ResponseEntity<>(commonResponse, status);
    }
}