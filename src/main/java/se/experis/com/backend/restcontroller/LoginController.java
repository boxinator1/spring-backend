package se.experis.com.backend.restcontroller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.JSONPObject;
import com.fasterxml.jackson.databind.util.JSONWrappedObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import se.experis.com.backend.enums.AccountType;
import se.experis.com.backend.enums.EmailType;
import se.experis.com.backend.model.*;
import se.experis.com.backend.repositories.AccountRepository;
import se.experis.com.backend.repositories.VerificationRepository;
import se.experis.com.backend.util.Emailer;
import se.experis.com.backend.util.JwtHandler;
import se.experis.com.backend.util.RequestHandler;
import se.experis.com.backend.util.SalterHasher;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Base64;

@CrossOrigin //REMOVE
@RestController
public class LoginController {
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private VerificationRepository verificationRepository;

    /**
     *
     * @param login
     * @param request
     * @return
     */
    @RequestMapping(value="/login", method= RequestMethod.POST)
    public ResponseEntity<CommonResponse> authenticate(@RequestBody Login login, HttpServletRequest request) throws IOException {
        HttpStatus status = HttpStatus.UNAUTHORIZED;
        CommonResponse commonResponse = new CommonResponse();
        String ipAddress = request.getRemoteAddr();

        if(!RequestHandler.getInstance().isBlackListed(ipAddress)) {
            String hashedLoginPW = SalterHasher.getEncryptedPassword(login.password);
            Account account = accountRepository.getByEmailAndPassword(login.email, hashedLoginPW);
            if(account != null) {
                if(account.accountType == AccountType.PENDING_USER){
                    commonResponse.message = "Please verify your email before logging in.";
                }
                else if(account.accountType != AccountType.GUEST) {
                    if(account.getIpAdresses().contains(ipAddress)) {
                        status = HttpStatus.ACCEPTED;
                        commonResponse.data = new AuthedAccount(account.accountType, JwtHandler.createAuthToken(account.id.toString(), ipAddress));
                    }
                    else {
                        Verification verification = new Verification();
                        verification.generateCode(true);
                        verification.setExpires(1.5);
                        account.verification = verification;
                        account = accountRepository.save(account);
                        try {
                            Emailer.getInstance().sendSpecialEmail(account.email, "Account verification", verification.code, EmailType.AUTH);
                            commonResponse.message = "Please verify with the code sent to your email.";
                            commonResponse.data = account.email;
                            status = HttpStatus.ACCEPTED;
                        } catch (IOException e) {
                            status = HttpStatus.INTERNAL_SERVER_ERROR;
                            commonResponse.message = "Failed to send email, please try again";
                        }
                    }

                }
            }
            else {
                RequestHandler.getInstance().addTry(ipAddress);
            }
        }
        else {
            commonResponse.message = "You are currently blacklisted.. Please try again in 15 minutes.";
        }

        return new ResponseEntity<>(commonResponse, status);
    }

    @RequestMapping(value="/login/verify", method=RequestMethod.POST)
    public ResponseEntity<CommonResponse> verifyAccountEmail(@RequestBody LoginVerification loginVerification, HttpServletRequest request) {
        CommonResponse commonResponse = new CommonResponse();
        String ipAddress = request.getRemoteAddr();
        HttpStatus status = HttpStatus.UNAUTHORIZED;
        Account account = accountRepository.getByEmail(loginVerification.email);
        if(account != null && account.verification != null){
            if(account.verification.code.equals(loginVerification.code)) {
                if(account.verification.hasExpired()) {
                    commonResponse.message = "Your token has expired";
                }
                else {
                    commonResponse.data = new AuthedAccount(account.accountType, JwtHandler.createAuthToken(account.id.toString(), ipAddress));
                    status = HttpStatus.ACCEPTED;
                }
                verificationRepository.deleteById(account.verification.getId());
                account.verification = null;
                account.addIpAdresses(ipAddress);
                accountRepository.save(account);
            }
        }

        return new ResponseEntity<>(commonResponse, status);
    }
    @ExceptionHandler(Exception.class)
    public ResponseEntity<CommonResponse> handleException() {
        CommonResponse commonResponse = new CommonResponse();
        commonResponse.message = "Server error";
        return new ResponseEntity<>(commonResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
