package se.experis.com.backend.restcontroller;


import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;
import se.experis.com.backend.enums.AccountType;
import se.experis.com.backend.enums.EmailType;
import se.experis.com.backend.enums.PackageStatus;
import se.experis.com.backend.model.*;
import se.experis.com.backend.model.Package;
import se.experis.com.backend.repositories.AccountRepository;
import se.experis.com.backend.repositories.PackageRepository;
import se.experis.com.backend.repositories.VerificationRepository;
import se.experis.com.backend.util.Emailer;
import se.experis.com.backend.util.AccountValidator;

import se.experis.com.backend.util.SalterHasher;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.UUID;

import java.util.*;
import java.util.stream.Collectors;

@CrossOrigin //REMOVE
@RestController
/**
 * Handling all APIS concering the accounts.
 */
public class AccountController {
    @Value("${spring.weburl}")
    private String WEBURL;

    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private VerificationRepository verificationRepository;
    @Autowired
    private PackageRepository packageRepository;

    /**
     * 17 September 2020 - Andreas Lif
     * This API will try to create a new account from the given Account object in the body.
     * It first validates the information using the {@link AccountValidator} class.
     * If the validation passes the API will proceed to create the account.
     *
     * A account is only counted as active if it has verified it's email. Therefore we create a {@link se.experis.com.backend.model.Verification} which is used
     * to change the accountType of the account.
     * We send a email using {@link se.experis.com.backend.util.Emailer} containing the id of the verification and the verification code.
     *
     * @STATUS-202 - Created is sent if everything passes successfully.
     * @STATUS-400 - Bad request is sent if the validator fails the account information or if the email already exists. The {@link se.experis.com.backend.model.CommonResponse} will contain a message describing the error
     * @STATUS-500 - Server error, this is sent if the email failed to send or if the save fails and it isn't due to duplication error.
     *
     * THE API WILL ROLLBACK UPON EXCEPTION. EG IT WILL DELETE ANY SAVED DATA.
     *
     * @param account - Body containing the account object.
     * @return - A response entity containing the {@link se.experis.com.backend.model.CommonResponse} which holds a data and an explaining message. Also the HTTP status.
     *
     */
    @Transactional(rollbackFor = IOException.class)
    @RequestMapping(value="/account", method=RequestMethod.POST)
    public ResponseEntity<CommonResponse> registerAccount(@RequestBody Account account, HttpServletRequest request) throws IOException{
        CommonResponse commonResponse = new CommonResponse();
        HttpStatus status;
        String validate = AccountValidator.getInstance().validateAccount(account);


        if(validate.isEmpty()) {
            account.password = SalterHasher.getEncryptedPassword(account.password);
            status = HttpStatus.ACCEPTED;
            account.accountType = AccountType.PENDING_USER;
            Verification verification = new Verification();
            verification.generateCode(false);
            account.verification = verification;
            Account existingAccount = accountRepository.getByEmail(account.email);
            if(existingAccount == null || existingAccount.accountType == AccountType.GUEST) {
                if(existingAccount != null) {
                    if (existingAccount.verification != null) {
                        verificationRepository.delete(existingAccount.verification);
                    }
                    account.id = existingAccount.id;
                    account.packages = existingAccount.packages;
                }
                account = accountRepository.save(account);
                String url = request.getRequestURL().toString();
                url = url.substring(0, url.indexOf(request.getRequestURI()));
                commonResponse.data = account;

                Emailer.getInstance().sendSpecialEmail(account.email, "Account verification", url+"/verify/"+account.verification.getId()+"/"+account.verification.code, EmailType.VERIFY);
                commonResponse.message = "Account sucessfully created, please verify your email";
            }
            else {
                commonResponse.message = "Email already exists";
                status = HttpStatus.BAD_REQUEST;
            }

        } else {
            commonResponse.message = validate;
            status = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<>(commonResponse, status);

    }

    /**
     * 24 September 2020 - Oskar Olsson
     *  THE API will return all accounts if the account_type is ADMINISTRATOR
     *
     * @Status-200 - If the request is successful
     * @Status-403 - If the request is made by an unauthorized user
     *
     * @return - A response entity containing the {@link se.experis.com.backend.model.CommonResponse} which holds a data and an explaining message. Also the HTTP status.
     */
    @Transactional(rollbackFor = Exception.class)
    @RequestMapping(value="/account", method=RequestMethod.GET)
    public ResponseEntity<CommonResponse> getAllAccounts(HttpServletRequest request) {
        System.out.println(request.getAttribute("account"));
        CommonResponse commonResponse = new CommonResponse();
        HttpStatus status;

            Account account = (Account) request.getAttribute("account");

            if (account != null && account.accountType == AccountType.ADMINISTRATOR) {
                ArrayList allAccounts = (ArrayList) accountRepository.findAll();
                commonResponse.data = allAccounts;
                status = HttpStatus.OK;
            } else {
                commonResponse.message = "Permission denied";
                status = HttpStatus.FORBIDDEN;
            }


        return new ResponseEntity<>(commonResponse, status);
    }

    /**
     * 24 September 2020 - Oskar Olsson
     *  THE API will return a single account based on id (UUID) or the current logged in user
     *
     * @Status-200 - If the requested account is found
     * @Status-401 - If the requester doesn't have permission to access the account.
     * @Status-404 - If the requested account is not found
     * @param id - UUID of the account
     *
     * @return - A response entity containing the {@link se.experis.com.backend.model.CommonResponse} which holds a data and an explaining message. Also the HTTP status.
     */
    @Transactional(rollbackFor = Exception.class)
    @RequestMapping(value={"/account/{id}", "/account/current"}, method=RequestMethod.GET)
    public ResponseEntity<CommonResponse> getAccount(@PathVariable(required = false) UUID id, HttpServletRequest request) {
        CommonResponse commonResponse = new CommonResponse();
        HttpStatus status = HttpStatus.UNAUTHORIZED;

        Account fetchedAccount;

        if(id != null) {
            fetchedAccount = accountRepository.getById(id);
        }
        else {
            fetchedAccount = (Account) request.getAttribute("account");
        }

        Account account = (Account) request.getAttribute("account");

        if (fetchedAccount != null && account != null && (account.accountType == AccountType.ADMINISTRATOR || account.id.equals(fetchedAccount.id))) {
            commonResponse.data = fetchedAccount;
            commonResponse.message = "Account with id " + id;
            status = HttpStatus.OK;
        } else if(fetchedAccount == null && account != null && account.accountType == AccountType.ADMINISTRATOR) {
            commonResponse.message = "No account with id: " + id + " found.";
            status = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<>(commonResponse, status);
    }

    /**
     * 24 September 2020 - Oskar Olsson
     *  THE API will try to update a given account (with a UUID path parameter) or the current logged in user
     *  Only certain properties of the entity are allowed to be updated.
     *
     * @Status-200 - If the given verification is valid and the account is updated.
     * @Status-401 - If the requester doesn't have permission to access the account.
     * @Status-400 - If the request body contains invalid data
     * @param id - UUID of the account
     *
     * @return - A response entity containing the {@link se.experis.com.backend.model.CommonResponse} which holds a data and an explaining message. Also the HTTP status.
     */
    @Transactional(rollbackFor = Exception.class)
    @RequestMapping(value={"/account/{id}", "/account/current"}, method=RequestMethod.PATCH)
    public ResponseEntity<CommonResponse> updateAccount(HttpServletRequest request, @RequestBody Account reqAccount, @PathVariable(required = false) UUID id) throws IOException {
        CommonResponse commonResponse = new CommonResponse();
        HttpStatus status = HttpStatus.UNAUTHORIZED;

        Account fetchedAccount;
        if(id != null) {
            fetchedAccount = accountRepository.getById(id);
        }
        else {
            fetchedAccount = (Account) request.getAttribute("account");
        }

        Account account = (Account) request.getAttribute("account");

        if(fetchedAccount != null && account != null && account.id.equals(fetchedAccount.id)) {
            if(reqAccount.password != null && !reqAccount.password.trim().isEmpty()) {
                fetchedAccount.password = SalterHasher.getEncryptedPassword(reqAccount.password);
            }
            if (reqAccount.email != null && !reqAccount.email.trim().isEmpty()) {
                fetchedAccount.email = reqAccount.email;
            }
            if (reqAccount.countryOfResidence != null && !reqAccount.countryOfResidence.trim().isEmpty()) {
                fetchedAccount.countryOfResidence = reqAccount.countryOfResidence;
            }
            if (reqAccount.firstName != null && !reqAccount.firstName.trim().isEmpty()) {
                fetchedAccount.firstName = reqAccount.firstName;
            }
            if (reqAccount.lastName != null && !reqAccount.lastName.trim().isEmpty()) {
                fetchedAccount.lastName = reqAccount.lastName;
            }
            if (reqAccount.zipCode > 0) {
                fetchedAccount.zipCode = reqAccount.zipCode;
            }
            if (reqAccount.contactNumber != null && !reqAccount.contactNumber.trim().isEmpty()) {
                fetchedAccount.contactNumber = reqAccount.contactNumber;
            }
            String validate = AccountValidator.getInstance().validateAccount(fetchedAccount);
            if (validate.isEmpty()) {
                commonResponse.message = "Account updated";
                status = HttpStatus.OK;
                commonResponse.data = accountRepository.save(fetchedAccount);
            } else {
                status = HttpStatus.BAD_REQUEST;
                commonResponse.message = validate;
            }
        }

        return new ResponseEntity<>(commonResponse, status);

    }

    /**
     * 24 September 2020 - Oskar Olsson
     *  THE API will delete a given account by it's id (UUID) if the requesting user  is ADMINISTRATOR
     *
     * @Status-200 - If the request is successful
     * @Status-403 - If the request is made by an unauthorized user or no token is present
     *
     * @return - A response entity containing the {@link se.experis.com.backend.model.CommonResponse} which holds a data and an explaining message. Also the HTTP status.
     */
    @Transactional(rollbackFor = Exception.class)
    @RequestMapping(value="/account/{id}", method=RequestMethod.DELETE)
    public ResponseEntity<CommonResponse> deleteAccount(HttpServletRequest request, @PathVariable UUID id) {

        CommonResponse commonResponse = new CommonResponse();
        HttpStatus status = HttpStatus.UNAUTHORIZED;

        Account account = (Account) request.getAttribute("account");
        if(account != null && account.accountType == AccountType.ADMINISTRATOR) {
            Account accountToDelete = accountRepository.getById(id);
            if (accountToDelete == null) {
                commonResponse.message = "User not found";
                status = HttpStatus.NOT_FOUND;
            }
            else {
                accountRepository.delete(accountToDelete);
                commonResponse.message = "Account successfully deleted";
                status = HttpStatus.ACCEPTED;
            }
        }

        return new ResponseEntity<>(commonResponse, status);
    }

    /**
     * 17 September 2020 - Andreas Lif
     *  THE API will validate the verification by first trying to get the {@link se.experis.com.backend.model.Verification} from it is in the DB.
     *  If a verification exists it will then compared the given code with the Verifications code. If both is equal we set the AccountType of the account linked
     *  to the verification to Registered and delete the given Verification.
     *
     * @Status-200 - If the given verification is valid and the account is updated.
     * @Status-400 - If the given verification is invalid.
     * @Status-500 - If the server fails to either save or delete the verification.
     * @param id - Id of the verification object
     * @param code - code of the verification object
     * @return - A response entity containing the {@link se.experis.com.backend.model.CommonResponse} which holds a data and an explaining message. Also the HTTP status.
     */
    @Transactional(rollbackFor = Exception.class)
    @RequestMapping(value="/verify/{id}/{code}", method=RequestMethod.GET)
    public RedirectView verifyAccountEmail(@PathVariable UUID id, @PathVariable String code) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(WEBURL);
        Verification verification = verificationRepository.getById(id);
        if(verification != null && verification.code.equals(code)) {
            Account account = verification.account;
            account.accountType = AccountType.REGISTERED_USER;
            account.verification = null;
            if(account.getPackages().size() > 0) {
                List<Package> packages =  account.getPackages().stream().map(p -> {
                    if(p.status == PackageStatus.PENDING) {
                        p.status = PackageStatus.CREATED;
                    }
                    return p;
                }).collect(Collectors.toList());
                packageRepository.saveAll(packages);
                account.packages = packages;
            }
            accountRepository.save(account);
            verificationRepository.deleteById(id);
            stringBuilder.append("verifiedlogin");

        }

        return new RedirectView(stringBuilder.toString());
    }

    /**
     * 1 October 2020 - Andreas Lif
     * This endpoint is dedicated to generate verification codes if a user wants a new password.
     *
     *
     * @Status-200 - If a verification already has been sent.
     * @Status-201 - If a code has been sent to the given email
     * @Status-404 - If the mail isnot found or the accountType is guest
     * @Status-401 - If the given account is PENDING_USER, eg hasnt verified their email yet.
     * @Status-500 - If the server fails to either save or delete the verification.
     *
     * @param email - The email address of the account requesting a new password.
     * @return - A response entity containing the {@link se.experis.com.backend.model.CommonResponse} which holds a data and an explaining message. Also the HTTP status.
     */
    @Transactional(rollbackFor = IOException.class)
    @RequestMapping(value="/account/lost", method=RequestMethod.POST)
    public ResponseEntity<CommonResponse> registerAccount(@RequestBody String email) throws IOException {
        CommonResponse commonResponse = new CommonResponse();
        HttpStatus status;

        Account account = accountRepository.getByEmail(email.toLowerCase());
        if(account == null || account.accountType == AccountType.GUEST) {
            status = HttpStatus.NOT_FOUND;
        } else {
            if (account.accountType == AccountType.PENDING_USER) {
                status = HttpStatus.UNAUTHORIZED;
                commonResponse.message = "This account hasnt verified its email.";
            } else if(account.verification != null && !account.verification.hasExpired()) {
                status = HttpStatus.OK;
                commonResponse.message = "This account already has a pending verification";
            } else {
                if(account.verification != null) {
                    verificationRepository.delete(account.verification);
                }

                status = HttpStatus.ACCEPTED;
                Verification verification = new Verification();
                verification.generateCode(false);
                verification.setExpires(60);
                account.verification = verification;
                account = accountRepository.save(account);
                Emailer.getInstance().sendSpecialEmail(account.email, "Lost password code", account.verification.code,EmailType.LOST);
                commonResponse.message = "An code has been sent to your email.";
            }
        }

        return new ResponseEntity<>(commonResponse, status);
    }

    /**
     * 1 October 2020 - Andreas Lif
     * This endpoint is dedicated to save a new password for a user who requested it.
     *
     *
     * @Status-201 - If the new password has been set
     * @Status-400 - If the verification has expired or if the password doesn't hold the specifications.
     * @Status-401 - If hte account doesn't exist or if the verification code doesn't match.
     *
     * @param lostPassword - A Pojo containing the email of the account, the new password, and the code to the verification
     * @return - A response entity containing the {@link se.experis.com.backend.model.CommonResponse} which holds a data and an explaining message. Also the HTTP status.
     */
    @RequestMapping(value="/account/newPassword", method=RequestMethod.POST)
    public ResponseEntity<CommonResponse> registerAccount(@RequestBody LostPassword lostPassword) throws IOException {
        CommonResponse commonResponse = new CommonResponse();
        HttpStatus status = HttpStatus.UNAUTHORIZED;

        Account account = accountRepository.getByEmail(lostPassword.email);

        if(account != null && account.verification != null && account.verification.code.equals(lostPassword.code)) {
            if(account.verification.hasExpired()) {
               status = HttpStatus.BAD_REQUEST;
               commonResponse.message = "Your code has expired";
            } else {
                account.password = lostPassword.password;
                String errorMsg = AccountValidator.getInstance().validateAccount(account);
                if(!errorMsg.isEmpty()) {
                    status = HttpStatus.BAD_REQUEST;
                    commonResponse.message = "Invalid password";
                } else {
                    account.password = SalterHasher.getEncryptedPassword(lostPassword.password);
                    verificationRepository.delete(account.verification);
                    account.verification = null;
                    accountRepository.save(account);
                    status = HttpStatus.ACCEPTED;
                    commonResponse.message = "Your password has been updated";
                }
            }
        }

        return new ResponseEntity<>(commonResponse, status);
    }

    /**
     * Get account byEmail
     * @param request
     * @return
     */
    @RequestMapping(value="/account/email/{email}", method=RequestMethod.GET)
    public ResponseEntity<CommonResponse> getAccount(@PathVariable() String email, HttpServletRequest request) {
        CommonResponse commonResponse = new CommonResponse();
        HttpStatus status = HttpStatus.UNAUTHORIZED;

        Account account = (Account) request.getAttribute("account");
        if(account.accountType == AccountType.ADMINISTRATOR) {
            Account fetchedAccount = accountRepository.getByEmail(email.toLowerCase());
            if (fetchedAccount != null) {
                commonResponse.data = fetchedAccount;
                commonResponse.message = "Account with email : " + email;
                status = HttpStatus.OK;
            } else if (fetchedAccount == null && account != null && account.accountType == AccountType.ADMINISTRATOR) {
                commonResponse.message = "No account with email: " + email + " found.";
                status = HttpStatus.NOT_FOUND;
            }
        }

        return new ResponseEntity<>(commonResponse, status);
    }
    @ExceptionHandler(Exception.class)
    public ResponseEntity<CommonResponse> handleException() {
        CommonResponse commonResponse = new CommonResponse();
        commonResponse.message = "Server error";
        return new ResponseEntity<>(commonResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
