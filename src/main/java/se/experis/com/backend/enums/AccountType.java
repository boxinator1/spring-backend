package se.experis.com.backend.enums;

/**
 * Enums for the different types of accounts. These determines what a Account is able to do and which API calls it has access to.
 */
public enum AccountType {
    GUEST,
    PENDING_USER,
    REGISTERED_USER,
    ADMINISTRATOR
}
