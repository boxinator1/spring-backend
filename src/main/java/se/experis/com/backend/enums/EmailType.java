package se.experis.com.backend.enums;

public enum EmailType {
    VERIFY,
    AUTH,
    LOST,
    PACKAGE
}
