package se.experis.com.backend.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import se.experis.com.backend.model.Verification;

import java.util.UUID;

public interface VerificationRepository extends JpaRepository<Verification, UUID>{
    Verification getById(UUID id);
}