package se.experis.com.backend.enums;

/**
 * Enums for the different package statuses.
 * Increase efficiency as each package needs a packageStatus
 */
public enum PackageStatus {
    PENDING,
    CREATED,
    RECEIVED,
    INTRANSIT,
    COMPLETED,
    CANCELLED
}
