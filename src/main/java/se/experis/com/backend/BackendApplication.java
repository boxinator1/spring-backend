package se.experis.com.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import se.experis.com.backend.util.Emailer;
import se.experis.com.backend.util.JwtHandler;

import java.io.IOException;

@SpringBootApplication
public class BackendApplication {

	public static void main(String[] args) {

		try {
			JwtHandler.init(7.0);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			System.exit(0);
		}
		SpringApplication.run(BackendApplication.class, args);
	}

}
