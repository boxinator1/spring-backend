package se.experis.com.backend.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import se.experis.com.backend.model.Account;

import java.util.UUID;

public interface AccountRepository extends JpaRepository<Account, UUID>{
    Account getById(UUID id);
    Account getByEmail(String email);
    Account getByEmailAndPassword(String email, String password);
}