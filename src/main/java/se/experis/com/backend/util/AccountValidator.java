package se.experis.com.backend.util;

import se.experis.com.backend.model.Account;

import java.util.Date;

public class AccountValidator {
    private static AccountValidator accountValidator = null;

    public static AccountValidator getInstance() {
        if(accountValidator == null) {
            accountValidator = new AccountValidator();
        }
        return accountValidator;
    }

    public String validateAccount(Account account) {
        StringBuilder stringBuilder = new StringBuilder();

        if(!isValidEmailAddress(account.email)){
            stringBuilder.append("Invalid email address \n");
        }
        if(!isValidContactNumber(account.contactNumber)) {
            stringBuilder.append("Invalid contact number \n");
        }
        if(account.firstName == null || account.firstName.length() < 2) {
            stringBuilder.append("Invalid first name \n");
        }
        if(account.lastName == null || account.lastName.length() < 2) {
            stringBuilder.append("Invalid last name \n");
        }
        if(!Countries.getInstance().contains(account.countryOfResidence)) {
            stringBuilder.append("Invalid country \n");
        }
        if(account.zipCode < 100) {
            stringBuilder.append("Invalid zipCode");
        }
        if(account.dateOfBirth == null || account.dateOfBirth.compareTo(new Date("01/01/1910")) < 0 || account.dateOfBirth.compareTo(new Date()) > 1 ) {
            stringBuilder.append("Invalid date");
        }
        //Password strength?

        return stringBuilder.toString();

    }

    private boolean isValidContactNumber(String contactNumber) {
        if(contactNumber == null) {return false;}
        int numberLength = contactNumber.length();
        return (numberLength == 12 && contactNumber.matches("\\+[0-9]*")) || (numberLength == 10 && contactNumber.matches("[0-9]*"));
    }
    private boolean isValidEmailAddress(String email) {
        if(email == null) { return false;}
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }
}
