package se.experis.com.backend.model;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;
import java.util.Random;
import java.util.UUID;

@Entity

@JsonIdentityInfo(
        generator = ObjectIdGenerators.UUIDGenerator.class,
        property = "id")
public class Verification {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(name = "id", updatable = false, nullable = false)
    private UUID id;

    public UUID getId() {
        return id;
    }

    @Column(nullable = false)
    public String code;

    @Column
    public Date expires;

    public void setExpires(double minutes) {
        this.expires = new Date((long) (System.currentTimeMillis() + (1000*60*minutes)));
    }

    public boolean hasExpired() {
        return this.expires.before(new Date());
    }

    @OneToOne(mappedBy = "verification")
    public Account account;

    public String generateCode(boolean numbers) {
        if(numbers) {
            code = String.valueOf((int) Math.floor(Math.random() * 8999)+1000);
        }
        else {
                int leftLimit = 48; // numeral '0'
                int rightLimit = 122; // letter 'z'
                Random random = new Random();
                int targetStringLength = (int) Math.floor(Math.random() * 25) + 15;

                code = random.ints(leftLimit, rightLimit + 1)
                        .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
                        .limit(targetStringLength)
                        .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                        .toString();
        }
        return code;
    }

}
