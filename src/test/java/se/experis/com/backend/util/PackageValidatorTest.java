package se.experis.com.backend.util;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import se.experis.com.backend.enums.PackageStatus;
import se.experis.com.backend.model.Package;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class PackageValidatorTest {

    private Package aPackage;
    @BeforeEach
    public void setUp() {
        aPackage = new Package();
        aPackage.account = null;
        aPackage.receiver = "Andreas";
        aPackage.boxColor = "#FFFFFF";
        aPackage.email = "andreas@he.com";
        aPackage.status = PackageStatus.CREATED;
        aPackage.date = new Date();
        aPackage.destination = "Sverige";
        aPackage.weight = (float) 1.48;
    }
    @Test
    void testAllValid() {
        assertEquals("", PackageValidator.getInstance().validatePackage(aPackage));
    }
    @Test
    void invalidEmail() {
        aPackage.email = "invalid@";
        assertTrue(PackageValidator.getInstance().validatePackage(aPackage).contains("email"));
    }
    @Test
    void invalidDest() {
        aPackage.destination = "invalid@";
        assertTrue(PackageValidator.getInstance().validatePackage(aPackage).contains("country"));
    }
    @Test
    void weight() {
        aPackage.weight = (float) 0;
        assertTrue(PackageValidator.getInstance().validatePackage(aPackage).contains("weight"));
    }
}