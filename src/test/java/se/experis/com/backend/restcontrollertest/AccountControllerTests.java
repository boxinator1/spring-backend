package se.experis.com.backend.restcontrollertest;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.ResultMatcher;
import se.experis.com.backend.enums.AccountType;
import se.experis.com.backend.model.Account;
import se.experis.com.backend.model.LostPassword;
import se.experis.com.backend.model.Verification;
import se.experis.com.backend.repositories.AccountRepository;
import se.experis.com.backend.util.Emailer;
import se.experis.com.backend.util.JwtHandler;
import se.experis.com.backend.util.SalterHasher;

import java.io.IOException;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.*;

import static org.assertj.core.api.Assertions.*;
import static org.hamcrest.Matchers.containsString;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.*;


import static org.springframework.test.web.client.match.MockRestRequestMatchers.content;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@AutoConfigureMockMvc
public class AccountControllerTests {
    final ObjectMapper objectMapper = new ObjectMapper();
    private Account account;
    private Account account2;
    private Account adminAccount;
    private String token;
    @Autowired
    private MockMvc mvc;

    @Mock
    private Emailer emailer;

    @MockBean
    private AccountRepository accountRepository;

    @BeforeEach
    void setUpAccount() throws Exception {

        adminAccount = new Account();
        adminAccount.email = "ogge@oggeelito.com";
        adminAccount.contactNumber = "0701231212";
        adminAccount.countryOfResidence = "Thailand";

        adminAccount.zipCode = 35230;
        adminAccount.firstName = "Pelle";
        adminAccount.lastName = "Larsson";
        adminAccount.dateOfBirth = new Date(System.currentTimeMillis() - 90000);
        adminAccount.accountType = AccountType.ADMINISTRATOR;

        account = new Account();
        account.email = "andreaslif.95@gmail.com";
        account.dateOfBirth = new Date(System.currentTimeMillis() - 90000);
        account.countryOfResidence = "Sverige";
        account.password = "maybe2";
        account.firstName = "Andreas";
        account.lastName = "Sven";
        account.contactNumber = "0708548994";
        account.zipCode = 35515;
        account.id = UUID.randomUUID();
        when(accountRepository.save(any())).thenAnswer(
                (InvocationOnMock invocation) -> saveAccount((Account) invocation.getArguments()[0]));
        setMock(emailer);
        JwtHandler.init(7);
        token = JwtHandler.createAuthToken(UUID.randomUUID().toString(), "127.0.0.1"); //IP address is the address every request sent with hte Mocking framework gets.

        //We don't want to waste emails on testing
        doNothing().when(emailer).sendEmail(anyString(), anyString(), anyString());
        doNothing().when(emailer).sendSpecialEmail(anyString(), anyString(), anyString(), any());
        when(accountRepository.getByEmail(any())).thenReturn(null);
    }
    private Account saveAccount(Account acc) {
        account2 = acc;
        return acc;
    }
    private void setMock(Emailer mock) {
        try {
            Field instance = Emailer.class.getDeclaredField("emailer");
            instance.setAccessible(true);
            instance.set(instance, mock);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @AfterEach
    public void resetSingleton() throws Exception {
        Field instance = Emailer.class.getDeclaredField("emailer");
        instance.setAccessible(true);
        instance.set(null, null);
    }

    @Test
    void shouldCreateUser() throws Exception {
        String accountString = objectMapper.writeValueAsString(account);
        accountString = accountString.substring(0, accountString.length()-1) + ",\"password\":\"ssssss\"}";
        MvcResult mvcResult = mvc.perform(post("/account")
                .contentType(MediaType.APPLICATION_JSON)
                .content(accountString))
                .andExpect(status().isAccepted())
                .andReturn();
        assertTrue(mvcResult.getResponse().getContentAsString().contains("andreaslif.95@gmail.com"));
    }

    @Test
    void shouldGetAllUsers() throws Exception {
        // Returns 200 if the request is  made by ADMININSTRATOR
        when(accountRepository.getById(any())).thenReturn(adminAccount);
        when(accountRepository.findAll()).thenReturn(new ArrayList<>());
        mvc.perform(get("/account")
                    .accept(MediaType.APPLICATION_JSON)
                    .header("authentication", token))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void shouldNotGetAllUsers() throws Exception {
        // Returns 403 if the request is not made by ADMININSTRATOR
        when(accountRepository.getById(any())).thenReturn(account);
       MvcResult mvcResult = mvc.perform(get("/account")
                .accept(MediaType.APPLICATION_JSON)
                .header("authentication", token))
                .andDo(print())
                .andExpect(status().isForbidden())
               .andReturn();
       assertTrue(mvcResult.getResponse().getContentAsString().contains("Permission denied"));
    }

    @Test
    void shouldGetSingleUser() throws Exception {

        account.id = UUID.randomUUID();
        token = JwtHandler.createAuthToken(account.id.toString(), "127.0.0.1"); //IP address is the address every request sent with hte Mocking framework gets.
        when(accountRepository.getById(account.id)).thenReturn(account);
        mvc.perform(get("/account/" + account.id)
                .accept(MediaType.APPLICATION_JSON)
                .header("authentication", token))
                    .andDo(print())
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.data.firstName").value("Andreas"))
                    .andReturn();
    }

    @Test
    void shouldUpdateSingleUser() throws Exception {
        Account updatedAccount = new Account();
        updatedAccount.email = "oskarsNewMail@mailermail.com";
        when(accountRepository.getById(any())).thenReturn(account);
        mvc.perform(patch("/account/" + account.id)
                .header("authentication", token)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updatedAccount)))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    void shouldNotUpdateSingleUser() throws Exception {
        Account updatedAccount = new Account();
        updatedAccount.email = "oskarsNewMail@@mailermail.com";
        when(accountRepository.getById(any())).thenReturn(account);
        MvcResult mvcResult = mvc.perform(patch("/account/" + account.id)
                .header("authentication", token)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updatedAccount)))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andReturn();
        assertTrue(mvcResult.getResponse().getContentAsString().contains("Invalid email"));
    }

    @Test
    void shouldGiveIncorrectEmail() throws Exception {
        account.email = "notok";
        MvcResult mvcResult = mvc.perform(post("/account")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(account)))
                .andExpect(status().isBadRequest())
                .andReturn();
        assertTrue(mvcResult.getResponse().getContentAsString().contains("Invalid email address"));
    }

    @Test
    void shouldGiveIncorrectCountry() throws Exception {
        account.countryOfResidence = "sweden";
        MvcResult mvcResult = mvc.perform(post("/account")

                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(account)))
                .andExpect(status().isBadRequest())
                .andReturn();
        assertTrue(mvcResult.getResponse().getContentAsString().contains("Invalid country"));
    }

    @Test
    void shouldGiveIncorrectNameAndLastName() throws Exception {
        account.firstName = "s";
        account.lastName = "m";
        MvcResult mvcResult = mvc.perform(post("/account")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(account)))
                .andExpect(status().isBadRequest())
                .andReturn();
        assertTrue(mvcResult.getResponse().getContentAsString().contains("Invalid first name"));
        assertTrue(mvcResult.getResponse().getContentAsString().contains("Invalid last name"));
    }

    @Test
    void shouldGiveIncorrectContactNumber() throws Exception {
        account.contactNumber = "5";
        MvcResult mvcResult = mvc.perform(post("/account")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(account)))
                .andExpect(status().isBadRequest())
                .andReturn();
        assertTrue(mvcResult.getResponse().getContentAsString().contains("Invalid contact"));
    }

    @Test
    void shouldFailIfEmailExists() throws Exception {
        String accountString = objectMapper.writeValueAsString(account);
        accountString = accountString.substring(0, accountString.length()-1) + ",\"password\":\"ssssss\"}";
        when(accountRepository.getByEmail(anyString())).thenReturn(account);
        MvcResult mvcResult = mvc.perform(post("/account")
                .contentType(MediaType.APPLICATION_JSON)
                .content(accountString))
                .andExpect(status().isBadRequest())
                .andReturn();
        assertTrue(mvcResult.getResponse().getContentAsString().contains("Email already exists"));
    }

    @Test
    void shouldGiveServerErrorIfEmailFailsToSend() throws Exception {
        doThrow(IOException.class).when(emailer).sendEmail(anyString(),anyString(),anyString());
        mvc.perform(post("/account")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(account)))
                .andExpect(status().is5xxServerError())
                .andReturn();
    }

    @Test
    void savedAccountShouldHaveVerification() throws Exception {
        String accountString = objectMapper.writeValueAsString(account);
        accountString = accountString.substring(0, accountString.length()-1) + ",\"password\":\"ssssss\"}";
        mvc.perform(post("/account")
                .contentType(MediaType.APPLICATION_JSON)
                .content(accountString))
                .andExpect(status().isAccepted())
                .andReturn();
        assertThat(account2.verification).isNotEqualTo(null);
    }

    @Test
    void sentEmailShouldContainVerificationCode() throws Exception {
        String accountString = objectMapper.writeValueAsString(account);
        accountString = accountString.substring(0, accountString.length()-1) + ",\"password\":\"ssssss\"}";
        System.out.println(accountString);
        mvc.perform(post("/account")
                .contentType(MediaType.APPLICATION_JSON)
                .content(accountString))
                .andExpect(status().isAccepted())
                .andReturn();

        doAnswer(
                invocation -> {
                    assertThat(invocation.getArguments()[0].toString()).isEqualTo(account.email);
                    assertThat(invocation.getArguments()[1].toString().contains(account2.verification.code));
                    return null;
                }
        ).when(emailer).sendEmail(anyString(), anyString(), anyString());
        assertThat(account2.verification).isNotEqualTo(null);
    }



    //LostPassword test

    @Test
    void sentEmailShouldContainCode() throws Exception {
        account.accountType = AccountType.REGISTERED_USER;
        when(accountRepository.getByEmail(anyString())).thenReturn(account);
        mvc.perform(post("/account/lost")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(account.email)))
                .andExpect(status().isAccepted())
                .andReturn();

        doAnswer(
                invocation -> {
                    assertThat(invocation.getArguments()[0].toString()).isEqualTo(account.email);
                    assertThat(invocation.getArguments()[1].toString().contains(account2.verification.code));
                    return null;
                }
        ).when(emailer).sendEmail(anyString(), anyString(), anyString());
        assertThat(account2.verification).isNotEqualTo(null);
    }

    @Test
    void aGuestUserShouldBeNotFound() throws Exception {
        account.accountType = AccountType.GUEST;
        when(accountRepository.getByEmail(anyString())).thenReturn(account);
        mvc.perform(post("/account/lost")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(account.email)))
                .andExpect(status().isNotFound());
    }
    @Test
    void aPendingUserShouldBeUnAuhtoriezed() throws Exception {
        account.accountType = AccountType.PENDING_USER;
        when(accountRepository.getByEmail(anyString())).thenReturn(account);
        mvc.perform(post("/account/lost")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(account.email)))
                .andExpect(status().isUnauthorized());
    }

    @Test
    void aFailedEmailShouldBe500() throws Exception {

        account.accountType = AccountType.REGISTERED_USER;
        doThrow(IOException.class).when(emailer).sendSpecialEmail(anyString(),anyString(),anyString(), any());
        when(accountRepository.getByEmail(anyString())).thenReturn(account);
        mvc.perform(post("/account/lost")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(account.email)))
                .andExpect(status().is5xxServerError());
    }

    @Test
    void shouldPassWhenCorrectAndSetNewPassword() throws Exception {
        account.accountType = AccountType.REGISTERED_USER;
        LostPassword lostPassword = new LostPassword();
        lostPassword.email = account.email;
        lostPassword.code = "any";
        lostPassword.password = "newpassword";

        Verification v = new Verification();
        v.code = "any";
        v.setExpires(90);
        v.account = account;

        account.verification = v;

        when(accountRepository.getByEmail(anyString())).thenReturn(account);
        mvc.perform(post("/account/newPassword")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(lostPassword)))
                .andExpect(status().isAccepted());

        assertEquals(SalterHasher.getEncryptedPassword(lostPassword.password), account2.password); //Comparing the saved account to the given password.
    }

    @Test
    void shouldFailWithBadCode() throws Exception {
        account.accountType = AccountType.REGISTERED_USER;
        LostPassword lostPassword = new LostPassword();
        lostPassword.email = account.email;
        lostPassword.code = "any";
        lostPassword.password = "newpassword";

        Verification v = new Verification();
        v.code = "an2y";
        v.setExpires(90);
        v.account = account;

        account.verification = v;

        when(accountRepository.getByEmail(anyString())).thenReturn(account);
        mvc.perform(post("/account/newPassword")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(lostPassword)))
                .andExpect(status().isUnauthorized());
        assertEquals(null, account2); //should be null since save is never called
    }
    @Test
    void shouldFailWithNullAccount() throws Exception {
        account.accountType = AccountType.REGISTERED_USER;
        LostPassword lostPassword = new LostPassword();
        lostPassword.email = account.email;
        lostPassword.code = "any";
        lostPassword.password = "newpassword";

        when(accountRepository.getByEmail(anyString())).thenReturn(null);
        mvc.perform(post("/account/newPassword")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(lostPassword)))
                .andExpect(status().isUnauthorized());
        assertEquals(null, account2); //should be null since save is never called
    }

    @Test
    void shouldBadRequestWithExpiredCode() throws Exception {
        account.accountType = AccountType.REGISTERED_USER;
        LostPassword lostPassword = new LostPassword();
        lostPassword.email = account.email;
        lostPassword.code = "any";
        lostPassword.password = "newpassword";

        Verification v = new Verification();
        v.code = "any";
        v.setExpires(-900);
        v.account = account;

        account.verification = v;

        when(accountRepository.getByEmail(anyString())).thenReturn(account);
        mvc.perform(post("/account/newPassword")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(lostPassword)))
                .andExpect(status().isBadRequest());
        assertEquals(null, account2); //should be null since save is never called
    }
}
