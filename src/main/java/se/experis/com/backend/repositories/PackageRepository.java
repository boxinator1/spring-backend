package se.experis.com.backend.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import se.experis.com.backend.enums.PackageStatus;
import se.experis.com.backend.model.Package;

import java.util.Date;

public interface PackageRepository extends JpaRepository<Package, Integer> {
    Package getAllByEmail (String email);
    Package getById (Integer id);
    Package getAllById (Date date);
    Package [] findAllByStatus (PackageStatus status);

}
