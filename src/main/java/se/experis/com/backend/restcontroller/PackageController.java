package se.experis.com.backend.restcontroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;
import se.experis.com.backend.enums.AccountType;
import se.experis.com.backend.enums.EmailType;
import se.experis.com.backend.enums.PackageStatus;
import se.experis.com.backend.model.*;
import se.experis.com.backend.model.Package;
import se.experis.com.backend.repositories.AccountRepository;
import se.experis.com.backend.repositories.CountryRepository;
import se.experis.com.backend.repositories.PackageRepository;
import se.experis.com.backend.repositories.VerificationRepository;
import se.experis.com.backend.util.Emailer;
import se.experis.com.backend.util.PackageValidator;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@CrossOrigin //TODO remove
@RestController
 /*
  Handling APIS concerning the Packages.
 */


public class PackageController {
    @Value("${spring.weburl}")
    private String WEBURL;
    @Autowired
    private PackageRepository packageRepository;
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private CountryRepository countryRepository;
    @Autowired
    private VerificationRepository verificationRepository;

    /**
     * This endpoint aims to create a new shipment.
     * The passed body is validated through PackageValidator, it checks for valid weight, country and email corresponding error messages are shown through CommonResponse.
     *
     * If there isn't a authenticated account it will proceed to check if the shipments email exists in a guest account. Otherwise it creates it.
     *
     * The 'id', 'date' and 'status' are all automatically inserted to the shipment data.
     *
     * @STATUS-202 - Created is sent if validator gives no error and body is passed correctly.
     * @STATUS-400 - Bad request is sent if weight country or email is invalid. The {@link se.experis.com.backend.model.CommonResponse} will contain a message describing the error
     * @STATUS-500 - Server error, this is sent if the email failed to send or if the save fails and it isn't due to duplication error.
     *
     * @param shipment - Body containing the shipment object.
     * @PackageStatus.CREATED - On creation of shipment, set status through the enum
     * @return - A response entity containing the {@link se.experis.com.backend.model.CommonResponse} which holds a data and an explaining message. Also the HTTP status.
     */
    @Transactional(rollbackFor = IOException.class)
    @RequestMapping(value = "/shipments", method = RequestMethod.POST)
    public ResponseEntity<CommonResponse> newShipment(@RequestBody Package shipment, HttpServletRequest request) throws IOException {
        HttpStatus status = HttpStatus.UNAUTHORIZED;
        CommonResponse commonResponse = new CommonResponse();
        Account account = (Account) request.getAttribute("account");

        if (account != null && account.accountType != AccountType.PENDING_USER) {
            shipment.email = account.email;
            String packageValidator = PackageValidator.getInstance().validatePackage(shipment);
            if (packageValidator.isEmpty()) {
                status = HttpStatus.CREATED;

                shipment.status = PackageStatus.CREATED;
                shipment.date = new Date();
                shipment.account = account;
                shipment = packageRepository.save(shipment);
                commonResponse.data = shipment;
                commonResponse.message = "Shipment was CREATED";
            } else {
                commonResponse.message = packageValidator;
                status = HttpStatus.BAD_REQUEST;
            }
        } else {
            if(shipment.email != null) {
                account = accountRepository.getByEmail(shipment.email);
                if((account != null && account.accountType != AccountType.GUEST)) {
                    commonResponse.message = "This email is already registered. Please login in order to send a package.";
                    status = HttpStatus.UNAUTHORIZED;
                    return new ResponseEntity<>(commonResponse, status);
                }
                else {
                    if(account != null) {
                        Optional<Package> aPackage = account.getPackages().stream().filter(p -> p.status == PackageStatus.PENDING).findAny();
                        if (aPackage.isPresent()) {
                            commonResponse.message = "Please verify the package you've already sent before sending a new one";
                            status = HttpStatus.UNAUTHORIZED;
                            return new ResponseEntity<>(commonResponse, status);
                        }
                    }
                    else {
                        account = new Account();
                        account.email = shipment.email;
                        account.accountType = AccountType.GUEST;
                        account.fillGuestAccount();
                    }
                    Verification verification = new Verification();
                    verification.generateCode(false);
                    account.verification = verification;
                    account = accountRepository.save(account);

                    shipment.status = PackageStatus.PENDING;
                    shipment.date = new Date();
                    shipment.account = account;
                    shipment = packageRepository.save(shipment);
                    String url = request.getRequestURL().toString();
                    url = url.substring(0, url.indexOf(request.getRequestURI()));
                    Emailer.getInstance().sendSpecialEmail(account.email, "Shipment verification", url+"/shipments/verify/"+account.verification.getId()+"/"+account.verification.code+"/"+shipment.id, EmailType.PACKAGE);
                    commonResponse.message = "Please verify your email for the shipment to be sent.";
                    status = HttpStatus.ACCEPTED;
                }
            }
            else {
                commonResponse.message = "No email was specified.";
                return new ResponseEntity<>(commonResponse, status);
            }
        }


        return new ResponseEntity<>(commonResponse, status);
    }

    @RequestMapping(value="shipments/verify/{id}/{code}/{pid}", method=RequestMethod.GET)
    public RedirectView verifyPackage(@PathVariable UUID id, @PathVariable String code, @PathVariable Integer pid) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(WEBURL);
        Verification verification = verificationRepository.getById(id);
        if(verification != null && verification.code.equals(code)) {
            Package shipment = packageRepository.getById(pid);
            if(shipment != null) {
                shipment.status = PackageStatus.CREATED;
                packageRepository.save(shipment);
                Account account = verification.account;
                account.verification = null;
                accountRepository.save(account);
                verificationRepository.deleteById(id);
                stringBuilder.append("verifiedorder");
            }
        }

        return new RedirectView(stringBuilder.toString());
    }

    @RequestMapping(value = "/shipments/cost", method = RequestMethod.POST)
    public ResponseEntity<CommonResponse> getShipmentCost(@RequestBody Package shipment, HttpServletRequest request) {
        HttpStatus status = HttpStatus.BAD_REQUEST;
        CommonResponse commonResponse = new CommonResponse();

        Account account = (Account) request.getAttribute("account");
        if(account == null) {
            if(shipment.email != null) {
                account = accountRepository.getByEmail(shipment.email);
                if((account != null && account.accountType != AccountType.GUEST)) {
                    commonResponse.message = "This email is already registered. Please login in order to send a package.";
                    status = HttpStatus.UNAUTHORIZED;
                    return new ResponseEntity<>(commonResponse, status);
                }
            }
            else {
                commonResponse.message = "No email was specified.";
                status = HttpStatus.UNAUTHORIZED;
                return new ResponseEntity<>(commonResponse, status);
            }
        }
        else {
            if(account.accountType == AccountType.PENDING_USER) {
                commonResponse.message = "Please verify your email before sending a package.";
                status = HttpStatus.UNAUTHORIZED;
                return new ResponseEntity<>(commonResponse, status);
            }
            else {
                Optional<Package> aPackage = account.getPackages().stream().filter(p -> p.status == PackageStatus.PENDING).findAny();
                if(aPackage.isPresent()) {
                    commonResponse.message = "Please verify the package you've already sent before sending a new one";
                    status = HttpStatus.UNAUTHORIZED;
                    return new ResponseEntity<>(commonResponse, status);
                }
            }
            shipment.email = account.email;
        }
        String errors = PackageValidator.getInstance().validatePackage(shipment);
        if(!errors.isEmpty()) {
            commonResponse.message = errors;
            return new ResponseEntity<>(commonResponse, status);
        }
        Optional<Country> country = countryRepository.findAll().stream().filter(c -> c.countryName.toLowerCase().equals(shipment.destination.toLowerCase())).findFirst();
        if(country.isEmpty()) {
            commonResponse.message = "We do not ship to (" + shipment.destination +")";
            return new ResponseEntity<>(commonResponse, status);
        }
        shipment.cost = (int) (shipment.weight * country.get().multiplier);
        status = HttpStatus.ACCEPTED;
        commonResponse.data = shipment;
        return new ResponseEntity<>(commonResponse, status);
    }

        /**
         * This endpoint aims to retrieve a list of all relevant and current shipments. It's depending on user/admin.
         * Listed in order of creation-date, in accordance with documentation.
         *
         * @STATUS-200 - OK if resource has been fetched and was transmitted in the message body.
         * @Status-401 - IF auth token is invalid.
         * @Status-204 - IF packages are null.
         */
        @RequestMapping(value = "/shipments", method = RequestMethod.GET)
        public ResponseEntity<CommonResponse> getAllShipments (HttpServletRequest request) {

            CommonResponse commonResponse = new CommonResponse();
            HttpStatus status = HttpStatus.UNAUTHORIZED;
            Account account = (Account) request.getAttribute("account");
            if(account != null && (account.accountType != AccountType.GUEST && account.accountType != AccountType.PENDING_USER)) {
                if (account.accountType == AccountType.ADMINISTRATOR) {
                    commonResponse.data = packageRepository.findAll();
                } else {
                    commonResponse.data = account.getPackages();
                }
                commonResponse.message = "All shipments displayed";
                status = HttpStatus.OK;
            }

            return new ResponseEntity<>(commonResponse, status);
        }

        /**
         * This endpoint aims to retrieve the information of 1 shipment.
         *
         * @STATUS-200 - OK if resource has been fetched and was transmitted in the message body.
         * @STATUS-400 - Bad Request if server can not and will not fulfill any requests.
         * @STATUS-404 - Not Found if the server can't find the requested resource
         *
         * @param id - Pathvariable containing the specific id.
         */
        @RequestMapping(value = "/shipments/{id}", method = RequestMethod.GET)
        public ResponseEntity<CommonResponse> getShipmentsById (@PathVariable Integer id, HttpServletRequest request){
            CommonResponse commonResponse = new CommonResponse();
            HttpStatus status = HttpStatus.UNAUTHORIZED;
            Account account = (Account) request.getAttribute("account");
            if(account != null && account.accountType == AccountType.ADMINISTRATOR) {

                Package shipment = packageRepository.getById(id);

                if (shipment != null) {
                    commonResponse.data = shipment;
                    commonResponse.message = "Shipment with Id: " + id;
                    status = HttpStatus.OK;

                } else {
                    commonResponse.message = "No Id on number: " + id;
                    status = HttpStatus.NOT_FOUND;
                }
            }
            return new ResponseEntity<>(commonResponse, status);
        }

        /**
         * This endpoint aims to update the status-information of 1 shipment.
         * As the requested body is defined as Enums, the body will be validated instantly. If body is correct, validate if a Package "shipment" on the path-Id exist.
         * Update the status according to RequestBody "newStatus".
         *
         * @param id - Pathvariable containing the id.
         * @newStatus - The new status of the shipment. Type PackageStatus
         */
        @RequestMapping(value = "/shipments/{id}", method = RequestMethod.PATCH)
        public ResponseEntity<CommonResponse> updateShipmentsById (@PathVariable Integer id, @RequestBody(required = false) PackageStatus newStatus, HttpServletRequest request){

            CommonResponse commonResponse = new CommonResponse();
            HttpStatus status = HttpStatus.UNAUTHORIZED;
            Account account = (Account) request.getAttribute("account");
            if(account != null) {
                Package shipment = packageRepository.getById(id);
                if(shipment == null) {
                    commonResponse.message = "No Id on number: " + id;
                    status = HttpStatus.NOT_FOUND;
                }
                else {
                    boolean unAuth = false;
                    if (newStatus == null && account.accountType == AccountType.REGISTERED_USER && shipment.account.id.equals(account.id)) {
                        shipment.status = PackageStatus.CANCELLED;
                    } else if (account.accountType == AccountType.ADMINISTRATOR) {
                        if(newStatus != null) {
                            shipment.status = newStatus;
                        }
                        else {
                            unAuth = true;
                            status = HttpStatus.BAD_REQUEST;
                            commonResponse.message = "You need to include the new status.";
                        }
                    }
                    else {
                        unAuth = true;
                    }
                    if(!unAuth) {
                        shipment = packageRepository.save(shipment);
                        commonResponse.message = "Successfully "+ shipment.status +" the shipment";
                        status = HttpStatus.ACCEPTED;
                    }
                }
            }
            return new ResponseEntity<>(commonResponse, status);
        }

        /**
         * This endpoint aims to delete 1 shipment according to Pathvariable id.
         *
         * @STATUS-200 - OK if resource was fetched and deleted.
         * @STATUS-404 - Not Found if the server can't find the requested resource
         *
         * @param id - Pathvariable containing the id.
         */
        @RequestMapping(value = "/shipments/{id}", method = RequestMethod.DELETE)
        public ResponseEntity<CommonResponse> updateShipmentsById (@PathVariable Integer id, HttpServletRequest request){

            CommonResponse commonResponse = new CommonResponse();
            HttpStatus status = HttpStatus.UNAUTHORIZED;
            Account account = (Account) request.getAttribute("account");
            if(account != null && account.accountType == AccountType.ADMINISTRATOR) {
                Package shipment = packageRepository.getById(id);

                if (shipment != null) {
                    Account pAccount = shipment.account;
                    pAccount.packages = pAccount.getPackages().stream().filter(p -> !p.id.equals(shipment.id)).collect(Collectors.toList());
                    accountRepository.save(pAccount);
                    packageRepository.delete(shipment);

                    commonResponse.message = "Package was deleted";
                    status = HttpStatus.OK;
                } else {
                    commonResponse.message = "No package with id number: " + id;
                    status = HttpStatus.NOT_FOUND;
                }
            }
            return new ResponseEntity<>(commonResponse, status);
        }

        /**
         * This endpoint aims to retrieve the information of all completed shipments.
         *
         * @STATUS-200 - OK if resource was fetched and transmitted in the message body.
         */
        @RequestMapping(value = "/shipments/complete", method = RequestMethod.GET)
        public ResponseEntity<CommonResponse> getCompletedShipments (HttpServletRequest request) {
            CommonResponse commonResponse = new CommonResponse();
            HttpStatus status = HttpStatus.UNAUTHORIZED;
            Account account = (Account) request.getAttribute("account");
            System.out.println("got this far?");
            if(account != null) {
                if(account.accountType == AccountType.ADMINISTRATOR) {
                    commonResponse.data = packageRepository.findAllByStatus(PackageStatus.COMPLETED);
                    commonResponse.message = "All users completed shipments are displayed";
                }
                else {
                    commonResponse.data = account.getPackages().stream().filter(p -> p.status == PackageStatus.COMPLETED).collect(Collectors.toList());
                    commonResponse.message = "All your completed shipments are displayed";
                }
                status = HttpStatus.OK;
            }

            return new ResponseEntity<>(commonResponse, status);
        }

        /**
         * This endpoint aims to retrieve the information of all cancelled shipments.
         *
         * @STATUS-200 - OK if resource was fetched and transmitted in the message body.
         */
        @RequestMapping(value = "/shipments/cancelled", method = RequestMethod.GET)
        public ResponseEntity<CommonResponse> getCancelledShipments (HttpServletRequest request) {
            CommonResponse commonResponse = new CommonResponse();
            HttpStatus status = HttpStatus.UNAUTHORIZED;
            Account account = (Account) request.getAttribute("account");

            if(account != null) {
                if(account.accountType == AccountType.ADMINISTRATOR) {
                    commonResponse.data = packageRepository.findAllByStatus(PackageStatus.CANCELLED);
                    commonResponse.message = "All users cancelled shipments are displayed";
                }
                else {
                    commonResponse.data = account.getPackages().stream().filter(p -> p.status == PackageStatus.CANCELLED).collect(Collectors.toList());
                    commonResponse.message = "All your cancelled shipments are displayed";
                }
                status = HttpStatus.OK;
            }

            return new ResponseEntity<>(commonResponse, status);
        }

        /**
         * This endpoint aims validate and show a shipment, if and only if it is already completed.
         *
         * @STATUS-200 - OK if resource was fetched and transmitted in the message body.
         *
         * @param id - Pathvariable containing the id.
         */
        @RequestMapping(value = "/shipments/complete/{id:[0-1]}", method = RequestMethod.GET)
        public ResponseEntity<CommonResponse> getShipmentCompleted (@PathVariable Integer id, HttpServletRequest request){

            CommonResponse commonResponse = new CommonResponse();
            HttpStatus status = HttpStatus.UNAUTHORIZED;
            Account account = (Account) request.getAttribute("account");

            if(account != null) {
                Package shipment = packageRepository.getById(id);
                status = HttpStatus.NOT_FOUND;
                commonResponse.message = "No completed shipment with following ID";
                if(shipment != null) {
                    if (shipment.status.equals(PackageStatus.COMPLETED)) {
                        if((account.accountType == AccountType.ADMINISTRATOR) || (account.accountType == AccountType.REGISTERED_USER && shipment.account.email.equals(account.email))) {
                            commonResponse.data = shipment;
                            commonResponse.message = "Details of the completed shipment";
                        }
                    }
                    status = HttpStatus.OK;
                }
            }
            return new ResponseEntity<>(commonResponse, status);
        }

    /**
     * Here we go into the completedShipments by using regex. We want it to start as a UUID if it should go into thiis api.
     * @param id - Id of the user
     * @param request - Request
     * @return - All packages of the account with the given id which are completed
     */
        @RequestMapping(value = "/shipments/complete/{id:[a-f0-9].*}", method = RequestMethod.GET)
        public ResponseEntity<CommonResponse> getCompletedShipmentsByCustomerId (@PathVariable UUID id, HttpServletRequest request){
            CommonResponse commonResponse = new CommonResponse();
            HttpStatus status = HttpStatus.UNAUTHORIZED;
            Account account = (Account) request.getAttribute("account");

            if(account != null && account.accountType == AccountType.ADMINISTRATOR) {
                Account getAcc = accountRepository.getById(id);
                status = HttpStatus.NOT_FOUND;
                commonResponse.message = "No account with the following id";
                if(getAcc != null) {
                    commonResponse.data = getAcc.getPackages().stream().filter(p -> p.status == PackageStatus.COMPLETED).collect(Collectors.toList());
                    status = HttpStatus.OK;
                }
            }
            return new ResponseEntity<>(commonResponse, status);
        }

        @ExceptionHandler(Exception.class)
        public ResponseEntity<CommonResponse> handleException() {
            CommonResponse commonResponse = new CommonResponse();
            commonResponse.message = "Server error";
            return new ResponseEntity<>(commonResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }
