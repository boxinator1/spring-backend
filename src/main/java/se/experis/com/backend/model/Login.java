package se.experis.com.backend.model;

/**
 * Helper class for authentication.
 */
public class Login {
    public String password;
    public String email;

    public void setEmail(String email) {
        this.email = email.toLowerCase();
    }
}
