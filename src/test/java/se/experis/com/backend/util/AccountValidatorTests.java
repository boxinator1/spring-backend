package se.experis.com.backend.util;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.util.Assert;
import se.experis.com.backend.model.Account;
import se.experis.com.backend.util.AccountValidator;

import java.io.IOException;
import java.util.Date;

import static org.junit.Assert.assertTrue;

public class AccountValidatorTests {

    private Account account;
    @BeforeEach
    void setUpAccount() {
        account = new Account();
        account.email = "andreaslif.95@gmail.com";
        account.dateOfBirth = new Date(System.currentTimeMillis() - 90000);
        account.countryOfResidence = "Sverige";
        account.firstName = "Andreas";
        account.lastName = "Sven";
        account.contactNumber = "0708548994";
        account.zipCode = 35515;
    }

    @Test
    public void shouldGiveEmptyStringWhenValidInput() {
        assertTrue(AccountValidator.getInstance().validateAccount(account).isBlank());
    }
    @Test
    public void shouldGiveMsgWhenInvalidEmail() {
        account.email = "abc@@h.com";
        assertTrue(AccountValidator.getInstance().validateAccount(account).contains("email"));
    }
    @Test
    public void shouldGiveMsgWhenInvalidCountry() {
        account.countryOfResidence = "InvalidCountry";
        assertTrue(AccountValidator.getInstance().validateAccount(account).contains("country"));
    }

    @Test
    public void shouldGiveMsgWhenInvalidFirstNameAndLastName() {
        account.firstName = "a";
        account.lastName = "b";
        assertTrue(AccountValidator.getInstance().validateAccount(account).contains("first name"));
        assertTrue(AccountValidator.getInstance().validateAccount(account).contains("last name"));
    }
    @Test
    public void shouldGiveMsgWhenInvalidcontactNumber() {
        account.contactNumber = "070"; //To short
        assertTrue(AccountValidator.getInstance().validateAccount(account).contains("contact"));
        account.contactNumber = "0ad"; //Letters
        assertTrue(AccountValidator.getInstance().validateAccount(account).contains("contact"));

    }

    @Test
    public void shouldGiveMsgWhenInvalidZipCode() {
        account.zipCode = 15; //To short
        assertTrue(AccountValidator.getInstance().validateAccount(account).contains("zip"));


    }
}
