package se.experis.com.backend.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CountriesTest {

    @Test
    public void shouldNotFailIfValidCountry() {
        assertTrue(Countries.getInstance().contains("Sverige"));
        assertTrue(Countries.getInstance().contains("Danmark"));
        assertTrue(Countries.getInstance().contains("Norge"));
        assertTrue(Countries.getInstance().contains("Finland"));
        assertTrue(Countries.getInstance().contains("storbritannien"));
        assertTrue(Countries.getInstance().contains("Kongo"));
    }

    @Test
    public void shouldBeFalseWhenInvalidCountry() {
        assertFalse(Countries.getInstance().contains("Gepoi"));
        assertFalse(Countries.getInstance().contains("Dnemark"));
        assertFalse(Countries.getInstance().contains("Senada"));
        assertFalse(Countries.getInstance().contains("Lambo"));
        assertFalse(Countries.getInstance().contains("BigCar"));
        assertFalse(Countries.getInstance().contains("Cango"));
    }

}