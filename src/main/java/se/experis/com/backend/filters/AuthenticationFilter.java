package se.experis.com.backend.filters;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import se.experis.com.backend.model.Account;
import se.experis.com.backend.repositories.AccountRepository;
import se.experis.com.backend.util.JwtHandler;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.UUID;

@Component
public class AuthenticationFilter implements Filter {

    @Autowired
    private AccountRepository accountRepository;
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        String token = httpRequest.getHeader("authentication");
        Account account = null;
        if(token != null) {
            String accountId = JwtHandler.getAccountId(token, httpRequest.getRemoteAddr());
            try {
                account = accountRepository.getById(UUID.fromString(accountId));
            }
            catch(Exception ignored){}
        }
        request.setAttribute("account", account);
        chain.doFilter(request, response);
    }

}
