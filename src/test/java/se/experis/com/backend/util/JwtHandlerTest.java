package se.experis.com.backend.util;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;

import org.mockito.junit.MockitoJUnitRunner;

import java.lang.reflect.Field;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(MockitoJUnitRunner.class)
class JwtHandlerTest {

    @BeforeEach
    /**
     * Here we go in and hange the static value so that we can change the duration.
     */
    public void init() throws Exception {
        Field field = JwtHandler.class.getDeclaredField("verifier");
        field.setAccessible(true);
        field.set(field, null);
        JwtHandler.init(7.0);
    }

    @Test
    public void ShouldReturnCorrectAccountId() {
        String t = JwtHandler.createAuthToken("TestAccountId", "127.0.0.1");

        assertEquals("TestAccountId", JwtHandler.getAccountId(t, "127.0.0.1"));

    }

    @Test
    public void shouldBeNullOnTwoDifferentIPS() {
        String t = JwtHandler.createAuthToken("TestAccountId", "127.0.1.1");

        assertNull(JwtHandler.getAccountId(t, "127.0.0.1"));

    }

    @Test
    public void shouldBeNullIfExpired() throws Exception {
        Field field = JwtHandler.class.getDeclaredField("verifier");
        field.setAccessible(true);
        field.set(field, null);
        JwtHandler.init(-0.0001);
        String t = JwtHandler.createAuthToken("TestAccountId", "127.0.0.1");
        assertNull(JwtHandler.getAccountId(t, "127.0.0.1"));
    }
}