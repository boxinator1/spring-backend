package se.experis.com.backend.util;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * A Singelton used to store countries named in sweidsh in a hashSet.
 * Reason for using this singleton is to validate country input.
 */
public class Countries {
    private static Countries countries = null;
    private final Set<String> countrySet;
    public Countries() {
        //String with the names of each country in the world in swedish. Each country is seperated by a #
        countrySet = Arrays.stream("#afghanistan#albanien#algeriet#andorra#angola#antigua och barbuda#argentina#armenien#australien#azerbajdzjan#bahamas#bahrain#bangladesh#barbados#belgien#belize#benin#bhutan#bolivia#bosnien och hercegovina#botswana#brasilien#brunei#bulgarien#burkina faso#burundi#centralafrikanska republiken#chile#colombia#comorerna#costa rica#cypern#danmark#demokratiska republiken kongo#djibouti#finland#dominica#dominikanska republiken#ecuador#egypten#ekvatorialguinea#el salvador#elfenbenskusten#eritrea#estland#etiopien#fiji#filippinerna#frankrike#förenade arabemiraten#gabon#gambia#georgien#ghana#grekland#grenada#guatemala#guinea#guinea-bissau#guyana#haiti#honduras#indien#indonesien#irak#iran#irland#island#israel#italien#jamaica#japan#jemen#jordanien#kambodja#kamerun#kanada#kap verde#kazakstan#kenya#kina#kirgizistan#kiribati#kongo#kosovo#kroatien#kuba#kuwait#laos#lesotho#lettland#libanon#liberia#libyen#liechtenstein#litauen#luxemburg#madagaskar#makedonien#malawi#malaysia#maldiverna#mali#malta#marocko#marshallöarna#mauretanien#mauritius#mexiko#mikronesien#moçambique#moldavien#monaco#mongoliet#montenegro#myanmar#namibia#nauru#nederländerna#nepal#nicaragua#niger#nigeria#nordkorea#norge#nya zeeland#oman#pakistan#palau#palestina#panama#papua nya guinea#paraguay#peru#polen#portugal#qatar#rumänien#rwanda#ryssland#saint kitts och nevis#saint lucia#saint vincent och grenadinerna#salomonöarna#samoa#san marino#são tomé och principe#saudiarabien#schweiz#senegal#serbien#seychellerna#sierra leone#singapore#slovakien#slovenien#somalia#spanien#sri lanka#storbritannien#sudan#surinam#swaziland#sverige#sydafrika#sydkorea#sydsudan#syrien#tadzjikistan#taiwan#tanzania#tchad#thailand#tjeckien#togo#tonga#trinidad och tobago#tunisien#turkiet#turkmenistan#tuvalu#tyskland#uganda#ukraina#ungern#uruguay#usa#uzbekistan#vanuatu#vatikanen#venezuela#vietnam#vitryssland#zambia#zimbabwe#österrike#östtimor".split("#")).collect(Collectors.toSet());
    }
    public static Countries getInstance() {
        if(countries == null) {
            countries = new Countries();
        }
        return countries;
    }

    public boolean contains(String country) {
        return country != null && !country.trim().isEmpty() && countrySet.contains(country.toLowerCase());
    }
}
