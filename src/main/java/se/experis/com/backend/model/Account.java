package se.experis.com.backend.model;

import com.fasterxml.jackson.annotation.*;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.GenericGenerator;
import se.experis.com.backend.enums.AccountType;

import javax.persistence.*;
import java.util.*;

@Entity

@JsonIdentityInfo(
        generator = ObjectIdGenerators.UUIDGenerator.class,
        property = "id")
public class Account {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    public UUID id;

    @Column(nullable = false)
    public String firstName;

    @Column(nullable = false)
    public String lastName;

    @Column(nullable = false, unique = true)
    public String email;

    public void setEmail(String email) {
        this.email = email.toLowerCase();
    }


    @Column(nullable = false)
    public String countryOfResidence;

    @Column(nullable = false)
    public int zipCode;

    @Column(nullable = false)
    public Date dateOfBirth;

    @Column(nullable = false)
    public AccountType accountType;

    @Column(nullable = false)
    public String contactNumber;

    @JsonIgnore
    @Column(nullable = false)
    public String password;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name="IpAdresses", joinColumns=@JoinColumn(name="id"))
    @Column(name="ipAdresses")
    @JsonIgnore
    public Set<String> ipAdresses;

    public void addIpAdresses(String ip) {
        if(ipAdresses == null) {
            ipAdresses = new HashSet<>();
        }
        ipAdresses.add(ip);
    }

    public Set<String> getIpAdresses() {
        if(ipAdresses == null) {
            return new HashSet<>();
        }
        return ipAdresses;
    }
    @JsonIgnore
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "verification_id", referencedColumnName = "id")
    public Verification verification;

    @JsonSetter
    public void setPassword(String password) {
        this.password = password;
    }

    public void addPackage(Package pack) {
        if(packages == null) {
            packages = new ArrayList<>();
        }
        packages.add(pack);
    }

    public List<Package> getPackages() {
        if(packages == null) {
            return new ArrayList<>();
        }
        return packages;
    }
    public void fillGuestAccount() {
        firstName = "";
        lastName ="";
        contactNumber ="";
        dateOfBirth = new Date();
        zipCode = -1;
        password = "";
        countryOfResidence = "";

    }
    @JsonIgnore
    @OneToMany(mappedBy = "account", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    public List<Package> packages;

    @JsonGetter
    public int numberOfPackages() {
        return getPackages().size();
    }
}


