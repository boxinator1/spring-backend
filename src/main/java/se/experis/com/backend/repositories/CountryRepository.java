package se.experis.com.backend.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import se.experis.com.backend.model.Country;

public interface CountryRepository extends JpaRepository <Country, Integer> {
    Country getCountryById(Integer id);
    Country getAllByCountryName(String country);
}
