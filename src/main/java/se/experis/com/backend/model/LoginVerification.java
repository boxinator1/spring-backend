package se.experis.com.backend.model;

import com.sun.istack.NotNull;

public class LoginVerification {
    @NotNull
    public String email;
    @NotNull
    public String code;
}
