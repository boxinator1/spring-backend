package se.experis.com.backend.setups;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import se.experis.com.backend.util.Emailer;

import javax.annotation.PostConstruct;

/**
 * This component is used to extract the properties from application.properties.
 */
@Component
public class EmailSetup {
    @Value("${spring.sendgrid.api-key}")
    private String api;

    @Value("${spring.sendgrid.sendEmail}")
    private String senderEmailer;

    @PostConstruct
    public void postConstruct() {
        Emailer.setUp(senderEmailer, api);
    }
}
