package se.experis.com.backend.model;

import java.util.UUID;

public class LostPassword {
    public String password;
    public String code;
    public String email;
}
