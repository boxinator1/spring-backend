package se.experis.com.backend.util;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Base64;

public class SalterHasher {
    /**
     * Util that:
     * When getEncryptedPassword is called it fetches the current salt. If there is no current salt (first time), it randomizes one.
     * Add the byted salt with the byted password, make it into  hash.
     * The hash will be into a string within the controller to store in DB.
     */

    public static String getEncryptedPassword(String password) throws IOException {
        byte[] salt =  "stefan".getBytes();
        KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, 65536, 128);
        SecretKeyFactory factory;
        try {
            factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
            try {
                return Base64.getEncoder().encodeToString(factory.generateSecret(spec).getEncoded());
            } catch (InvalidKeySpecException e) {
                return "InvalidKeySpecException error";
            }
        } catch (NoSuchAlgorithmException e) {
            return "NoSuchAlgorithmException error";
        }
    }

}






