package se.experis.com.backend.util;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Base64;

import static org.junit.jupiter.api.Assertions.*;

public class SalterHasherTest {

    // Known inputs and outputs
    private static String password;
    private static String expectedPassword;

    // This is what we want: SHA256(salt+password);
    public static final String saltedHash = "1NJ5VyphSdFtHTGTKoyTcQ==";

    @Test
    public void testCorrectPasswordHash() throws IOException {
        password = "corncob";
        assertEquals(saltedHash, SalterHasher.getEncryptedPassword(password));
    }

    @Test
    public void shouldGiveTwoSimilarHash() throws IOException {
        password = "corncob";
        expectedPassword = "corncob";
        password = SalterHasher.getEncryptedPassword(password);
        expectedPassword = SalterHasher.getEncryptedPassword(expectedPassword);
        assertEquals(expectedPassword, password);
    }

    @Test
    public void shouldStillGiveTwoSimilarHashes_WithoutBaseEncoder() throws IOException {
        password = "corncob";
        expectedPassword = "corncob";
        assertEquals(SalterHasher.getEncryptedPassword(password), SalterHasher.getEncryptedPassword(expectedPassword));
    }

    @Test
    public void shouldNotGiveTwoSimilarHash() throws IOException {
        password = "corncob";
        expectedPassword = "coooooorncob";
        password = SalterHasher.getEncryptedPassword(password);
        expectedPassword = SalterHasher.getEncryptedPassword(expectedPassword);
        assertNotEquals(expectedPassword, password);
    }
}
