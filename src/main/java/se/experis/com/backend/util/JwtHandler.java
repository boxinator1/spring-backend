package se.experis.com.backend.util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;

import javax.xml.bind.DatatypeConverter;
import java.io.*;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Date;

public class JwtHandler {
    private static JWTVerifier verifier = null;
    private static Algorithm algorithm;
    private static double tokenDurationDays;


    public static void init(double durationDays) throws Exception {
        if(verifier == null) {
            RSAPrivateKey privateKey = (RSAPrivateKey) loadKey(true);
            RSAPublicKey publicKey = (RSAPublicKey) loadKey(false);
            algorithm = Algorithm.RSA256(publicKey, privateKey);
            verifier = JWT.require(algorithm)
                    .withIssuer("auth0")
                    .build();
            tokenDurationDays = durationDays;
        }
    }
    private static Object loadKey(boolean privateKey) throws Exception {
        try {
            InputStream inputStream = JwtHandler.class.getClassLoader().getResourceAsStream("keys/" + (privateKey ? "private" : "public") + "_key.pem");
            if(inputStream == null) {
                throw new FileNotFoundException("The " + (privateKey ? "private key" : "public key") + " was not found. Please generate one in the resources/keys.");
            }
            byte[] bytes = inputStream.readAllBytes();
            String data = new String(bytes);
            int start = data.indexOf("\n");
            int end = data.lastIndexOf("\n");
            byte[] encoded = DatatypeConverter.parseBase64Binary(data.substring(start+1, end));

            if(privateKey) {
                return KeyFactory.getInstance("RSA").generatePrivate(new PKCS8EncodedKeySpec(encoded));
            }
            return KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(encoded));
        }
        catch(OutOfMemoryError | IOException e) {
            throw new Exception("No memory. Please check the size of your keys and make sure the format is RSA");
        }
        catch(IllegalArgumentException e) {
            throw new Exception("Failed to parse the " + (privateKey ? "private key" : "public key") +". Please make sure the format is RSA.");
        }
        catch(NoSuchAlgorithmException e) {
            throw new Exception("Wrong key format on the " + (privateKey ? "private key" : "public key") + ". Please make sure its RSA.");
        }

    }

    public static String createAuthToken(String accountId, String ipAddress) {
        return JWT.create()
                .withExpiresAt(new Date((System.currentTimeMillis()+((int) (86400000*tokenDurationDays)))))
                .withIssuer("auth0")
                .withClaim("ip_address", ipAddress)
                .withClaim("account_id", accountId)
                .sign(algorithm);
    }

    public static String getAccountId(String token, String ipAddress) {
        try {
            DecodedJWT jwt = verifier.verify(token);
            if(jwt.getClaim("ip_address").asString().equals(ipAddress)) {
                return jwt.getClaim("account_id").asString();
            }
            return null;
        }
        catch(Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }
}

