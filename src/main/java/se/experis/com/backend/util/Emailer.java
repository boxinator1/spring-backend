package se.experis.com.backend.util;

import com.sendgrid.*;
import com.sendgrid.helpers.mail.Mail;
import com.sendgrid.helpers.mail.objects.Content;
import com.sendgrid.helpers.mail.objects.Email;
import com.sendgrid.helpers.mail.objects.Personalization;
import se.experis.com.backend.enums.EmailType;

import java.io.IOException;
public class Emailer {

    public static Emailer emailer = null;
    private final SendGrid sendGrid;
    private boolean useTemplate = true;
    private static String sendMail = "";
    private static String apiKey = "";

    public Emailer() {
        sendGrid = new SendGrid(apiKey);
    }

    /**
     * Used to set the apikey and emailer from the properties.
     * @param sendAddress
     * @param api
     */
    public static void setUp(String sendAddress, String api) {
        if(sendMail.isEmpty() && apiKey.isEmpty()) {
            apiKey = api;
            sendMail = sendAddress;
        }
    }

    public static Emailer getInstance() {
        if(emailer == null) {
            emailer = new Emailer();
        }
        return emailer;
    }

    /**
     * Uses the sendgrid api to send an email
     * @param reciever - Email address of the one recieving the email.
     * @param subject - The subject, eg header of the email
     * @param content - Contents of the email.
     * Throws error upon failed sending of email.
     */
    public void sendEmail(String reciever, String subject, String content) throws IOException {
        Mail mail = new Mail(new Email(sendMail), subject, new Email(reciever), new Content("text/plain", content));
        Request request = new Request();
        request.setMethod(Method.POST);
        request.setEndpoint("mail/send");
        request.setBody(mail.build());
        Response resp = sendGrid.api(request);
        if(resp.getStatusCode() != 202) {
            throw new IOException();
        }
    }

    /**
     * Uses the sendgrid api to send an email
     * @param reciever - Email address of the one recieving the email.
     * @param subject - The subject, eg header of the email
     * @param content - Contents of the email.
     * Throws error upon failed sending of email.
     */
    public void sendSpecialEmail(String reciever, String subject, String content, EmailType emailType) throws IOException {
        if(!useTemplate) {
            sendEmail(reciever, subject, content);
            return;
        }
        Mail mail = new Mail();
        mail.setFrom(new Email(sendMail));
        mail.setSubject(subject);
        Email to = new Email(reciever);
        Personalization personalization = new Personalization();
        personalization.addTo(to);

        personalization.addDynamicTemplateData("email", reciever);
        switch(emailType) {
            case VERIFY:
                mail.setTemplateId("d-bccdd629945f4d3a9b43cae191b2dd99");
                personalization.addDynamicTemplateData("url", content);
                break;
            case LOST:
                mail.setTemplateId("d-f9a8e18c3ea34284a7ee4cddfa3faae9");
                personalization.addDynamicTemplateData("code", content);
                break;
            case AUTH:
                mail.setTemplateId("d-afc7e1c789704bfeab1e5366f1a531bb");
                personalization.addDynamicTemplateData("code", content);
                break;
            case PACKAGE:
                mail.setTemplateId("d-63442acb8c8f4125b35fae8f1a5f4338");
                personalization.addDynamicTemplateData("url", content);
                break;
        }




        mail.addPersonalization(personalization);

        Request request = new Request();
        request.setMethod(Method.POST);
        request.setEndpoint("mail/send");
        request.setBody(mail.build());
        Response resp = sendGrid.api(request);
        if(resp.getStatusCode() != 202) {
            throw new IOException();
        }
    }

}
