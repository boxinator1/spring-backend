package se.experis.com.backend.util;


import com.sendgrid.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
class EmailerTest {
    @Mock
    private SendGrid sendGrid;

    @InjectMocks
    private Emailer emailer;
    @BeforeEach
    public void initMocks() {
        MockitoAnnotations.initMocks(this);

    }
    @Test
    void shouldthrowwhenResponseStatusIsnt202() throws IOException {
        Response response = new Response();
        response.setStatusCode(58);
        doReturn(response).when(sendGrid).api(any());
        assertThrows(IOException.class, () -> emailer.sendEmail("andreashe@he.com", "AnySubject", "someContent"));
    }
    @Test
    void shouldNotThrowWhenResponseStatusIs202() throws IOException {
        Response response = new Response();
        response.setStatusCode(202);
        when(sendGrid.api(any())).thenReturn(response);
        assertDoesNotThrow(() -> emailer.sendEmail("andreashe@he.com", "AnySubject", "someContent"));
    }
}