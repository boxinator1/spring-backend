package se.experis.com.backend.model;

import se.experis.com.backend.enums.AccountType;

public class AuthedAccount {
    public AuthedAccount(AccountType type, String token) {
        this.token = token;
        this.type = type;
    }
    public AccountType type;
    public String token;
}
