package se.experis.com.backend.util;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.jupiter.api.Assertions.*;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

@RunWith(MockitoJUnitRunner.class)
class RequestHandlerTest {

    private HashMap<String, ArrayList<Date>> triesList;
    private Field triesField;
    private int maxTries;
    private RequestHandler requestHandler;

    @BeforeEach
    public void setUp() throws NoSuchFieldException, IllegalAccessException {
        requestHandler = new RequestHandler();
        triesField = requestHandler.getClass().getDeclaredField("triesList");
        triesList = new HashMap<>();
        triesField.setAccessible(true);
        Field f = requestHandler.getClass().getDeclaredField("MAX_TRIES");
        f.setAccessible(true);
        maxTries = f.getInt(requestHandler);


    }

    @Test
    void shouldContainIpWhenAdded() throws IllegalAccessException {
        System.out.println("Should contain ip");
        requestHandler.addTry("127.0.0.1");
        triesList = (HashMap<String, ArrayList<Date>>) triesField.get(requestHandler);
        assertTrue(triesList.containsKey("127.0.0.1"));
    }

    @Test
    void shouldAddToBlackListAfterMaxTries() {
        System.out.println("Testing should be at blacklist");
        for(int i = 0; i <= maxTries; i++) {
            requestHandler.addTry("127.0.0.1");
        }
        assertTrue(requestHandler.isBlackListed("127.0.0.1"));
    }

    @Test
    //Since we check the time of all previous tries each time we add a new try the first try will be removed.
    //Will only work if MAX_TRIES >= 1, since otherwise the
    void shouldRemoveTriesAfterTimeout() throws IllegalAccessException {

        //Lets add two new entries in th arrayList which both should be removed if check open. EG they should be before hte current date.
        ArrayList<Date> testingArrayList = new ArrayList<>();
        testingArrayList.add(new Date(12));
        testingArrayList.add(new Date(12));
        int newSize = testingArrayList.size();
        triesList.put("127.0.0.1", testingArrayList);
        triesField.set(requestHandler, triesList);
        //We now modify the actual hashmap in the requestHandler with out arraylist. We then check so that they actually were added.

        triesList = (HashMap<String, ArrayList<Date>>) triesField.get(requestHandler);
        assertEquals(newSize, triesList.get("127.0.0.1").size());

        //Now that they were added we want to add a new entry using addTry and make sure that our previous ones were removed. Eg. It should be one entry left.
        requestHandler.addTry("127.0.0.1");
        triesList = (HashMap<String, ArrayList<Date>>) triesField.get(requestHandler);
        assertEquals(1, triesList.get("127.0.0.1").size());
    }

    @Test
    void shouldNotBeBlacklistedAfterTimeout() throws NoSuchFieldException, IllegalAccessException {
        //Here we manually go in and add a blacklisting
        HashMap<String, Date> blackListing = new HashMap<>();
        blackListing.put("127.0.0.1", new Date(15));
        Field blackListingField = requestHandler.getClass().getDeclaredField("blackList");
        blackListingField.setAccessible(true);
        blackListingField.set(requestHandler, blackListing);
        blackListing = (HashMap<String,Date>) blackListingField.get(requestHandler);
        //Check so that the blacklistingExists
        assertNotEquals(null, blackListing.get("127.0.0.1"));
        //Now we check if it's blacklisted. Which should be false since it will be removed since the time has passed.

        assertFalse(requestHandler.isBlackListed("127.0.0.1"));

    }
}