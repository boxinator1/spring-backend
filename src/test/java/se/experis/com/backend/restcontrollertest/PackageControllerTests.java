package se.experis.com.backend.restcontrollertest;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.invocation.InvocationOnMock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import se.experis.com.backend.enums.AccountType;
import se.experis.com.backend.model.Account;
import se.experis.com.backend.model.Country;
import se.experis.com.backend.model.Package;
import se.experis.com.backend.repositories.AccountRepository;
import se.experis.com.backend.repositories.CountryRepository;
import se.experis.com.backend.repositories.PackageRepository;
import se.experis.com.backend.util.JwtHandler;

import java.util.ArrayList;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
@SpringBootTest
@AutoConfigureMockMvc
public class PackageControllerTests {

    final ObjectMapper objectMapper = new ObjectMapper();
    private String token;
    private ArrayList<Country> countries;
    private Account account;
    @Autowired
    private MockMvc mvc;

    @MockBean
    private AccountRepository accountRepository;
    @MockBean
    private PackageRepository packageRepository;
    @MockBean
    private CountryRepository countryRepository;

    private Package aPackage;
    private Package aPackage2;


    @BeforeEach
    public void setUp() throws Exception {

        aPackage = new Package();
        aPackage.boxColor = "#151656";
        aPackage.destination = "Sverige";
        aPackage.receiver = "Any";
        aPackage.weight = (float) 1.48;

        account = new Account();
        account.email = "andreas@hotmail.com";
        account.accountType = AccountType.REGISTERED_USER;
        when(packageRepository.save(any())).thenAnswer(
                (InvocationOnMock invocation) -> savePackage((Package) invocation.getArguments()[0]));
        JwtHandler.init(7.0);
        token = JwtHandler.createAuthToken(UUID.randomUUID().toString(), "127.0.0.1"); //IP address is the address every request sent with hte Mocking framework gets.

        countries = new ArrayList<>();
        Country swe = new Country();
        swe.multiplier = (float) 2.5;
        swe.countryName = "Sverige";

        countries.add(swe);
        when(countryRepository.findAll()).thenReturn(countries);
    }

    /*  This BeforeEach does not seem to work.. Would eliminate one line per @Test.
    @BeforeEach
    public void getCorrectAccountDetails(){
        when(accountRepository.getById(any())).thenReturn(account);
    }*/

    private Package savePackage(Package savedPackage) {
        aPackage2 = savedPackage;
        return savedPackage;
    }

    /**
     * Test that validates creation of package by correct body.
     * Expected:  201 Created
     */
    @Test
    void correct_shouldCreatePackage() throws Exception {
        when(accountRepository.getById(any())).thenReturn(account);

        MvcResult mvcResult = mvc.perform(post("/shipments")
                .header("authentication", token)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(aPackage)))
                .andExpect(status().isCreated())
                .andReturn();

        String content = mvcResult.getResponse().getContentAsString();
        Assert.assertTrue(content.contains("Sverige"));
        Assert.assertTrue(content.contains("andreas@hotmail.com"));

        //Check aPackage2 since it was update when the new package was saved.
        assertEquals(aPackage2.email, account.email);
    }

    /**
     * Test that validates whether the weight in the sent body is a negative value.
     * Expected: 400 Bad_request
     */
    @Test
    void negativeWeight_ShouldNotCreatePackage() throws Exception {
        when(accountRepository.getById(any())).thenReturn(account);

        aPackage.weight = (float) -0.5;

        MvcResult mvcResult = mvc.perform(post("/shipments")
                .header("authentication", token)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(aPackage)))
                .andExpect(status().isBadRequest())
                .andReturn();

        String content = mvcResult.getResponse().getContentAsString();
        Assert.assertTrue(content.contains("Invalid weight"));
    }

    /**
     * Test that validates whether the weight in the sent body is to large of a value. (Borders: 0->10000)
     * Expected: 400 Bad_request
     */
    @Test
    void tooLargeWeight_ShouldNotCreatePackage() throws Exception {
        when(accountRepository.getById(any())).thenReturn(account);

        aPackage.weight = (float) 10001;

        MvcResult mvcResult = mvc.perform(post("/shipments")
                .header("authentication", token)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(aPackage)))
                .andExpect(status().isBadRequest())
                .andReturn();

        String content = mvcResult.getResponse().getContentAsString();
        Assert.assertTrue(content.contains("Invalid weight"));
    }

    /**
     * Test that validates whether the boxColor in the sent body is a of correct HEX-style ("^#[0-9a-fA-F]{6}$").
     * Expected:  400 Bad_request
     */
    @Test
    void wrongColor_ShouldNotCreatePackage() throws Exception {
        when(accountRepository.getById(any())).thenReturn(account);

        aPackage.boxColor = "#FF45AAA"; //7 characters after the #

        MvcResult mvcResult = mvc.perform(post("/shipments")
                .header("authentication", token)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(aPackage)))
                .andExpect(status().isBadRequest())
                .andReturn();

        String content = mvcResult.getResponse().getContentAsString();
        Assert.assertTrue(content.contains("Invalid color"));
    }

    /**
     * Test that validates whether the boxColor in the sent body is a of correct HEX-style ("^#[0-9a-fA-F]{6}$").
     * Expected:  201 Created
     */
    @Test
    void correctColor_ShouldCreatePackage() throws Exception {
        when(accountRepository.getById(any())).thenReturn(account);

        aPackage.boxColor = "#F55faA";

        MvcResult mvcResult = mvc.perform(post("/shipments")
                .header("authentication", token)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(aPackage)))
                .andExpect(status().isCreated())
                .andReturn();

        String content = mvcResult.getResponse().getContentAsString();
        Assert.assertTrue(content.contains("Shipment was CREATED"));
    }

    /**
     * Test that validates whether the destination in the sent body is correct according to the list.
     * Expected:  400 Bad_request
     */
    @Test
    void wrongDestination_ShouldNotCreatePackage() throws Exception {
        when(accountRepository.getById(any())).thenReturn(account);

        aPackage.destination = "london";

        MvcResult mvcResult = mvc.perform(post("/shipments")
                .header("authentication", token)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(aPackage)))
                .andExpect(status().isBadRequest())
                .andReturn();

        String content = mvcResult.getResponse().getContentAsString();
        Assert.assertTrue(content.contains("Invalid country"));
    }

    /**
     * Test that validates whether the destination in the sent body is correct according to the list.
     * Expected:  201 Created
     */
    @Test
    void correctDestination_ShouldCreatePackage() throws Exception {
        when(accountRepository.getById(any())).thenReturn(account);

        aPackage.destination = "guinea-bissau";

        MvcResult mvcResult = mvc.perform(post("/shipments")
                .header("authentication", token)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(aPackage)))
                .andExpect(status().isCreated())
                .andReturn();

        String content = mvcResult.getResponse().getContentAsString();
        Assert.assertTrue(content.contains("Shipment was CREATED"));
    }

    /**
     * Test that validates whether the email in the sent body matches the correct style.
     * Expected:  400 Bad_request
     */
    @Test
    void wrongEmail_ShouldNotCreatePackage() throws Exception {
        when(accountRepository.getById(any())).thenReturn(account);

        account.email = "stefan123@Joppe@gmail.com"; //Too many @'s

        MvcResult mvcResult = mvc.perform(post("/shipments")
                .header("authentication", token)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(aPackage)))
                .andExpect(status().isBadRequest())
                .andReturn();

        String content = mvcResult.getResponse().getContentAsString();
        Assert.assertTrue(content.contains("Invalid email address"));
    }

    /*
     * Test for caluclating cost
     */
    @Test
    void shouldGiveCostWithNoUser() throws Exception {
        aPackage.destination = "sverige";
        aPackage.email = "any@he.com";
        aPackage.weight = (float) 1;
        when(accountRepository.getById(any())).thenReturn(null);


        MvcResult mvcResult = mvc.perform(post("/shipments/cost")
                .header("authentication", token)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(aPackage)))
                .andExpect(status().isAccepted())
                .andReturn();

        String content = mvcResult.getResponse().getContentAsString();
        Assert.assertTrue(content.contains("\"cost\":2")); //Since 2.5 * 1 rounded down is 1
    }

    @Test
    void shouldGiveCostWithUser() throws Exception {
        aPackage.destination = "sverige";
        aPackage.weight = (float) 1;
        when(accountRepository.getById(any())).thenReturn(account);


        MvcResult mvcResult = mvc.perform(post("/shipments/cost")
                .header("authentication", token)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(aPackage)))
                .andExpect(status().isAccepted())
                .andReturn();

        String content = mvcResult.getResponse().getContentAsString();
        Assert.assertTrue(content.contains("\"cost\":2")); //Since 2.5 * 1 rounded down is 1
    }
    @Test
    void shouldWorkWithGuestAccountType() throws Exception {
        aPackage.destination = "sverige";
        aPackage.weight = (float) 1;
        account.accountType = AccountType.GUEST;
        when(accountRepository.getById(any())).thenReturn(account);


        MvcResult mvcResult = mvc.perform(post("/shipments/cost")
                .header("authentication", token)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(aPackage)))
                .andExpect(status().isAccepted())
                .andReturn();

        String content = mvcResult.getResponse().getContentAsString();
        Assert.assertTrue(content.contains(account.email));
    }
    @Test
    void shouldGiveErrorWhenPendingUser() throws Exception {
        aPackage.destination = "sverige";
        aPackage.weight = (float) 1;
        account.accountType = AccountType.PENDING_USER;
        when(accountRepository.getById(any())).thenReturn(account);


        MvcResult mvcResult = mvc.perform(post("/shipments/cost")
                .header("authentication", token)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(aPackage)))
                .andExpect(status().isUnauthorized())
                .andReturn();

        String content = mvcResult.getResponse().getContentAsString();
        Assert.assertTrue(content.contains("verify your email"));
    }
    @Test
    void shouldGiveERrorIfCountryIsNotInDb() throws Exception {
        aPackage.destination = "norge";
        aPackage.weight = (float) 1;
        account.accountType = AccountType.GUEST;
        when(accountRepository.getById(any())).thenReturn(account);


        MvcResult mvcResult = mvc.perform(post("/shipments/cost")
                .header("authentication", token)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(aPackage)))
                .andExpect(status().isBadRequest())
                .andReturn();

        String content = mvcResult.getResponse().getContentAsString();
        Assert.assertTrue(content.contains("do not ship"));
    }

    @Test
    void shouldGiveErrorWhenNoEmailOrAccount() throws Exception {
        aPackage.destination = "Sverige";
        aPackage.weight = (float) 1;
        when(accountRepository.getById(any())).thenReturn(null);


        MvcResult mvcResult = mvc.perform(post("/shipments/cost")
                .header("authentication", token)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(aPackage)))
                .andExpect(status().isUnauthorized())
                .andReturn();

        String content = mvcResult.getResponse().getContentAsString();
        Assert.assertTrue(content.contains("No email"));
    }
}
