# Boxinator Backend
Below the instructions for installing and using the backend of the application Boxinator is presented. 
## Requiered installations
The following programs, libraries, languages is requiered to be downloaded and installed. <br>
**PostgresSql** - https://www.postgresql.org/download/ <br>
**Java** - https://www.java.com/sv/download/ <br>
**IDE(Intelliji)** -  https://www.jetbrains.com/idea/download/#section=windows <br>
**Git** - https://git-scm.com/downloads <br>

You're free to use any IDE but we recommend Intelliji.

## Using the application

After installing the above programs you need to get the application code. The easiest way to do this is to use gitbash.
1. Locate to a folder in which you want the application to be located.
2. Open git bash in the folder.
3. Type git clone ........ in Git Bash then press enter.
4. After the cloning is done open the project with Intelliji.
5. When the project is loaded, open the file located in src->resources->application.properties. 
6. In this file we are going to enter our SENDGRID apiKeys, the mail used in SendGrid and also the location of our databse.
7. Visit https://sendgrid.com/ and follow their guide in order to get a ApiKey and an email.
8. Now enter the apiKey and email into the application.properties opened in step 5.
9. Now that the apiKey is inserted Open pgAdmin4 which was installed when PostgresSql was installed.
10. Now you need to insert the database name in the application properties and also the scheme name.
11. Edit the lines named DATABASENAMEHERE with your database name and SCHEMENAMEHERE with your scheme name.
12. Now you can edit the WEBURL field to the address of your web address to the frontend.
13. Now that everything is set save the file.
14. (Optional) 2 pem keys are provided in the resources->keys folder, if you want to generate new ones follow the howTo.txt file.
15. Now everything is done and you can start your application.
16. (optional) You can enable templates in the emailer folder, it's set to true by default. You need to edit your template ids. More info about it on sendgrids webpage.
## Front end
In order to configure the frontend download it from the following link and follow the instructions provided.


### Boxinator Database ERD
![erd](Boxinator_ERD.jpg)

Boxinator was created by - Andreas, Oskar, Jacob. 2020-10-09